print("=========game load========")
CurCardGame = CardGame:Create()
CardGameState = {}
Players = {}
function TestMode(cardGamePlayer)
    if DebugMode then
        cardGamePlayer.player.coin = cardGamePlayer.player.coin + 10
    end
end
--士兵默认卡组
function Card:CreateSoldierCardGroup()
    local cards={}
    --for i = 1, 5 do
    --    table.insert(cards,Card:Create(CardsConfig[38]))
    --    table.insert(cards,Card:Create(CardsConfig[27]))
    --    --table.insert(cards,Card:Create(CardsConfig[28]))
    --    --table.insert(cards,Card:Create(CardsConfig[7]))
    --    table.insert(cards,Card:Create(CardsConfig[146]))
    --end
    --table.insert(cards,Card:Create(CardsConfig[25]))

    for i = 1, 4 do
       table.insert(cards,Card:Create(CardsConfig[1]))
       table.insert(cards,Card:Create(CardsConfig[2]))
    end
    table.insert(cards,Card:Create(CardsConfig[3]))
    --测试
    table.insert(cards,Card:Create(CardsConfig[6]))
    table.insert(cards,Card:Create(CardsConfig[24]))
    table.insert(cards,Card:Create(CardsConfig[18]))

    ----霰弹枪
    --table.insert(cards,Card:Create(CardsConfig[5]))
    ----恶意中伤
    --table.insert(cards,Card:Create(CardsConfig[3]))
    ----手榴弹
    --table.insert(cards,Card:Create(CardsConfig[6]))
    ----闪光弹
    --table.insert(cards,Card:Create(CardsConfig[8]))
    ----致盲射击
    --table.insert(cards,Card:Create(CardsConfig[4]))
    return cards
end
CardGameMonsters.OnTakeDamageEnd:Register(function(cardGamePlayer)
    Invoke(function(_cardGamePlayer)
        if CardGameMonsters:CheckAllDeath(cardGamePlayer) then
            CurCardGame:RoundOver(cardGamePlayer)
            return
        end
        CurCardGame:MonsterRoundEnd(_cardGamePlayer)
    end,0.5,cardGamePlayer)
end)


--call
function RoundStart(call,args)
    if call then
        args=splitstr_tostring(args)
        local player=Game.GetTriggerEntity()
        local cardGamePlayer = Players[player.index]
        --TestMode(cardGamePlayer)
        if args[1]=="Event" then
            local eventId=tonumber(args[2])
            local eventPos=tonumber(args[3])
            CardGameEvent.OnStart(cardGamePlayer,eventId,eventPos)
        else
            local level=args[1]
            player = player:ToPlayer()
            CurCardGame:Start(cardGamePlayer,level)
        end
    end
end



function AddCard(call)
    if call then
        local player=Game.GetTriggerEntity()
        player = player:ToPlayer()
        local cardGamePlayer = Players[player.index]
        cardGamePlayer.HandCard:AddCard(cardGamePlayer)
    end
end
Card.OnAddCard:Register(function(cardGamePlayer,copyCard,card)
    cardGamePlayer:SignalPlus("ReturnHandCardCount".. cardGamePlayer.HandCard.count)
    cardGamePlayer:SignalPlus("AddCard"..card:ToString()..","..cardGamePlayer.HandCard.count)
end)

CurCardGame.OnCardGameInit:Register(function(cardGamePlayer,level)
    print("========OnCardGameInit========")
    cardGamePlayer.player:SetThirdPersonFixedView(-90,45,300,800)
    cardGamePlayer.player.maxspeed = 0.01
    cardGamePlayer.RoundCard:Init(cardGamePlayer.CardKnapsack)
    cardGamePlayer.RoundCard:Init(cardGamePlayer.CardKnapsack)
    --回合初始化时召唤怪物，后续的回合怪物不会变动
    CardGameMonsters:Clear()
    CardGameMonsters.OnCreate:Trigger(cardGamePlayer,level)

    cardGamePlayer.HandCard.onUseCard:Register(CardGamePlayerUseCard,REGISTER_MODE.only)
    cardGamePlayer.HandCard.onUsedCard:Register(CardGamePlayerUsedCard,REGISTER_MODE.only)
    cardGamePlayer.OnDeath:Register(function(p)
        CurCardGame:RoundOver(p)
        p.player.maxspeed = 1
        p.player.maxhealth = 100
        p.player.health = 100
        p.health =  p.player.health
        Players[p.player.index] = CardGamePlayer:Create(p.player,OccupationEnum.Soldier)
    end)
    CardGameState[cardGamePlayer.index].value = GameStateEnum.Default
end)

function CardGamePlayerUseCard(cardGamePlayer,card)
    print("原来费用"..cardGamePlayer.cost)
    cardGamePlayer.cost = cardGamePlayer.cost - card.cost
    print("玩家"..cardGamePlayer.name)
    print("使用"..card.name.."消耗"..card.cost)
    print("剩余费用"..cardGamePlayer.cost)
    print("card.buff.id"..card.buff.id)
    print("card.buff.layer"..card.buff.layer)
    Card.OnUse:Trigger(cardGamePlayer,card)
end
function CardGamePlayerUsedCard(cardGamePlayer,card)
    local entities ={}
    if card.cardTarget==CardTargetEnum.Single then
        local monsterIndex=cardGamePlayer:GetTargetIndex(CardGameMonsters)
        local cardGameMonster= CardGameMonsters:GetByIndex(monsterIndex)
        entities[monsterIndex] = cardGameMonster
    elseif card.cardTarget == CardTargetEnum.Multiple then
        entities = CardGameMonsters:GetAliveAll()
    elseif card.cardTarget == CardTargetEnum.Self then
        --TODO 对自己类型攻击
        table.insert(entities,cardGamePlayer)
    end
    for entityIndex, cardGameEntity in pairs(entities) do
        local damage = card.damage
        cardGamePlayer.attackCount = card.attackCount
        cardGamePlayer:TakeDamage(cardGameEntity,damage,card.buff)
    end
    Card.OnUsed:Trigger(cardGamePlayer,card,entities)

    if card.armor>0 then
        cardGamePlayer:AddArmor(card.armor)
    end
    --1.5秒后检测怪物是否死亡，因为攻击会延迟最多1s后才造成死亡
    Invoke(function()
        if CardGameMonsters:CheckAllDeath(cardGamePlayer) then
            CurCardGame:RoundOver(cardGamePlayer)
        end
    end,1.5)
end
--抽卡阶段
CurCardGame.OnRoundStart:Register(function(cardGamePlayer,roundNum,level)
    print("=========OnRoundStart========")
    cardGamePlayer.targetIndex = 1
    local buff = cardGamePlayer:GetBuff(BuffEnum.Recovery)
    if buff then
        --回复不吃重伤
        cardGamePlayer:AddHealth(buff.layer)
    end

    buff = cardGamePlayer:GetBuff(BuffEnum.Poison)
    if buff then
        --中毒不吃重伤所以单独计算
        local damage = CardGameEntity.Hurt(cardGamePlayer,buff.layer)
        cardGamePlayer.player.health = cardGamePlayer.health
        cardGamePlayer:SignalPlus("PlayerAttacked")
    end
    cardGamePlayer:RoundStart()
    CardGameMonsters:RoundStart()
    print("========OnRoundStart========")
    for i = 1, 5 do
        cardGamePlayer.HandCard:AddCard(cardGamePlayer)
    end
    cardGamePlayer:SignalPlus("PlayerCost".. cardGamePlayer.cost..",".. cardGamePlayer:GetMaxCost())
    PrintStackCard(cardGamePlayer.HandCard.cards)
    CardGameMonsters:StorageAction(cardGamePlayer,roundNum,level)
    --for i = 1, CardGameMonsters:Count() do
    --    local monster= CardGameMonsters:GetByIndex(i)
    --    cardGamePlayer:SignalPlus("ReturnTargetInfo"..monster:ToString()..","..i)
    --end
    CardGameState[cardGamePlayer.index].value = GameStateEnum.RoundStartState
    Invoke(function(_cardGamePlayer)
        CurCardGame:PlayerAttack(_cardGamePlayer)
    end,0.5,cardGamePlayer)
end)
--玩家攻击阶段
CurCardGame.OnPlayerAttack:Register(function(cardGamePlayer)
    print("=========OnPlayerAttack========")
    CardGameState[cardGamePlayer.index].value = GameStateEnum.PlayerAttackState
end)
--玩家回合结束
CurCardGame.OnPlayerRoundEnd:Register(function(cardGamePlayer)
    print("========OnPlayerRoundEnd========")
    cardGamePlayer.HandCard:RemoveCards(cardGamePlayer.RoundCard)
    cardGamePlayer:HasTarget(false)
    CardGameState[cardGamePlayer.index].value = GameStateEnum.PlayerRoundEndState
    Invoke(function(_cardGamePlayer)
        CurCardGame:MonsterAttack(_cardGamePlayer)
    end,0.5,cardGamePlayer)
end)
--怪物攻击阶段
CurCardGame.OnMonsterAttack:Register(function(cardGamePlayer)
    print("========OnMonsterAttack========")
    --结算中毒伤害
    local cardGameMonsters = CardGameMonsters:GetAliveAll()
    for entityIndex, cardGameEntity in pairs(cardGameMonsters) do
        local buff = cardGameEntity:GetBuff(BuffEnum.Recovery)
        if buff then
            --回复不吃重伤
            local damage = CardGameEntity.Hurt(cardGameEntity,-buff.layer)
            cardGamePlayer:SignalPlus("ShowDamage".. entityIndex ..","..damage)
            cardGamePlayer:SignalPlus("ReturnTargetInfo".. cardGameEntity:ToString())
        end
        buff = cardGameEntity:GetBuff(BuffEnum.Poison)
        if buff then
            --中毒不吃重伤所以单独计算
            local damage = CardGameEntity.Hurt(cardGameEntity,buff.layer)
            if cardGameEntity.health == 0 then
                cardGameEntity.OnDeath:Trigger(cardGameEntity)
            end
            cardGamePlayer:SignalPlus("ShowDamage".. entityIndex ..","..damage)
            cardGamePlayer:SignalPlus("ReturnTargetInfo".. cardGameEntity:ToString())
        end
    end

    if CardGameMonsters:CheckAllDeath(cardGamePlayer) then
        CurCardGame:RoundOver(cardGamePlayer)
        return
    end

    CardGameMonsters:TakeDamage(cardGamePlayer)
    --cardGamePlayer:SignalPlus("PlayerAttacked"..cardGamePlayer.cost..","..cardGamePlayer.maxCost)
    CardGameState[cardGamePlayer.index].value = GameStateEnum.MonsterAttackState
end)
--
CurCardGame.OnMonsterAttacked:Register(function(cardGamePlayer)
    print("=========OnMonsterAttacked========")
    if CardGameMonsters:CheckAllDeath(cardGamePlayer) then
        CurCardGame:RoundOver(cardGamePlayer)
    end
end)
--怪物攻击阶段结束
CurCardGame.OnMonsterRoundEnd:Register(function(cardGamePlayer)
    print("=========OnMonsterRoundEnd========")
    Invoke(function(_cardGamePlayer)
        if _cardGamePlayer.death then
            return
        end
        CurCardGame:RoundStart(_cardGamePlayer)
    end,0.5,cardGamePlayer)

    CardGameState[cardGamePlayer.index].value = GameStateEnum.MonsterRoundEndState
end)
CurCardGame.OnRoundOver:Register(function(cardGamePlayer,level)
    print("=========OnRoundOver========")
    cardGamePlayer.extraCost = 0
    cardGamePlayer.HandCard:RemoveCards(cardGamePlayer.RoundCard)
    CardGameMonsters:Clear()
    CardGameState[cardGamePlayer.index].value = GameStateEnum.RoundOverState
    --cardGamePlayer.player:SetFirstPersonView()
    cardGamePlayer:AddCoin(math.random(10,20))
    cardGamePlayer:RemoveAllBuff()
end)

Framework.GamePlug.OnPlayerJoiningSpawn:Register(function(player)
    Players[player.index] = CardGamePlayer:Create(player,OccupationEnum.Soldier)
    CardGameState[player.index] = Game.SyncValue.Create("CardGameState"..player.index)
end)

Framework.GamePlug.OnPlayerDisconnect:Register(function(player)
    CardGameState[player.index] = nil
    Players[player.index] = nil
end)

Framework.GamePlug.OnPlayerSignalPlus:Register(function(player,msg)
    local outArgs={}
    if Framework.GetSignalPlusArgs(msg,"Control",outArgs)   then
        local cardGamePlayer = Players[player.index]
        local cardControlEnum = outArgs.numArgs[1]
        local handCard = cardGamePlayer.HandCard
        if cardGamePlayer.hasTarget then
            cardGamePlayer:SelectTarget(cardControlEnum, CardGameMonsters)
            print("player:GetTargetIndex()"..cardGamePlayer:GetTargetIndex(CardGameMonsters))
            local curCard=cardGamePlayer.HandCard:GetCurCard():Copy()
            if not curCard then return end
            local handCardIndex = cardGamePlayer.HandCard:GetCurCardIndex()
            Card.OnSelected:Trigger(cardGamePlayer,curCard)
            cardGamePlayer:SignalPlus("ReturnTargetIndex"..curCard:ToString()..","..handCardIndex..","..cardGamePlayer:GetTargetIndex(CardGameMonsters))
        else
            if handCard.cards.count==0 then return end
            cardGamePlayer.HandCard:SelectCard(cardControlEnum)
            cardGamePlayer:SignalPlus("SelectCard")
        end
    end
end)
Framework.GamePlug.OnPlayerSignalPlus:Register(function(player,msg)
    local outArgs={}
    if msg=="GetUseCard"  then
        local cardGamePlayer= Players[player.index]
        if cardGamePlayer.HandCard.cards.count>0 then
            local useCardIndex = cardGamePlayer.HandCard:GetCurCardIndex()
            if not cardGamePlayer.HandCard:UseCard(cardGamePlayer) then
                return
            end
            cardGamePlayer:SignalPlus("PlayerCost"..cardGamePlayer.cost..","..cardGamePlayer:GetMaxCost())
            --使用卡牌永远是0:移到弃牌堆(动画)
            cardGamePlayer:SignalPlus("ReturnUseCard"..useCardIndex..","..cardGamePlayer.HandCard.cards.count..","..0)
        end
    end
    if msg=="GetTargetsCount"  then
        player:SignalPlus("ReturnTargetsCount".. CardGameMonsters:Count())
    end
    if msg=="GetTargetsInfo"  then
        print("GetTargetsInfo!!!!!!!")
        for i = 1, CardGameMonsters:Count() do
            local monster= CardGameMonsters:GetByIndex(i)
            player:SignalPlus("ReturnTargetInfo"..monster:ToString())
        end
    end
    if Framework.GetSignalPlusArgs(msg,"HasTarget",outArgs)  then
        local cardGamePlayer = Players[player.index]
        if outArgs.strArgs[1]=="true" then
            cardGamePlayer:HasTarget(true)
        else
            cardGamePlayer:HasTarget(false)
        end
    end
    if msg=="GetHandCardCount" then
        local cardGamePlayer = Players[player.index]
        local handCard = cardGamePlayer.HandCard
        print("GetHandCardCount"..handCard.cards.count)
        cardGamePlayer:SignalPlus("ReturnHandCardCount"..handCard.cards.count)
    end
    if msg=="GetCurCard" then
        local cardGamePlayer = Players[player.index]
        local curCard=cardGamePlayer.HandCard:GetCurCard()
        if  not curCard then return end
        curCard=curCard:Copy()
        if not curCard then return end
        local handCardIndex = cardGamePlayer.HandCard:GetCurCardIndex()
        Card.OnSelected:Trigger(cardGamePlayer,curCard)
        cardGamePlayer:SignalPlus("ReturnCurCard"..curCard:ToString()..","..handCardIndex)
    end
    --第二次鼠标左键点击，使用卡牌之前
    if msg=="GetTargetIndex" then
        local cardGamePlayer = Players[player.index]
        local useCardIndex = cardGamePlayer.HandCard:GetCurCardIndex()
        if cardGamePlayer.startChoice then
            cardGamePlayer:SignalPlus("ChoiceCard"..useCardIndex)
            return
        end
        if not cardGamePlayer.HandCard then
            return
        end
        local curCard=cardGamePlayer.HandCard:GetCurCard():Copy()
        if not curCard then return end
        local handCardIndex = cardGamePlayer.HandCard:GetCurCardIndex()
        Card.OnSelected:Trigger(cardGamePlayer,curCard)
        cardGamePlayer:SignalPlus("ReturnTargetIndex"..curCard:ToString()..","..handCardIndex..","..cardGamePlayer:GetTargetIndex(CardGameMonsters))
    end
    if msg == "RoundEnd" then
        local cardGamePlayer = Players[player.index]
        CurCardGame:PlayerRoundEnd(cardGamePlayer)
    end
    if Framework.GetSignalPlusArgs(msg,"EndChoice",outArgs)   then
        local cardGamePlayer = Players[player.index]
        local args=outArgs.numArgs
        cardGamePlayer.startChoice = false
        for i = 1, #args do
            Invoke(function()
                local useCardIndex = args[i]
                local toWhere =  cardGamePlayer.HandCard.toWhere
                local card=cardGamePlayer.HandCard:GetByIndex(useCardIndex)
                local roundCard = cardGamePlayer.RoundCard
                --0:移到弃牌堆，
                if toWhere==0 then
                    --放入弃牌堆
                    roundCard.DiscardPile:Push(card)
                elseif toWhere==1 then
                    --放入抽牌堆
                    roundCard.DrawPiles:Push(card)
                elseif toWhere==2 then
                    --放入不可用的牌
                    roundCard.DeathCards:Push(card)
                end
                cardGamePlayer.HandCard:RemoveCardByIndex(useCardIndex)
                cardGamePlayer:SignalPlus("ReturnUseCard"..useCardIndex..","..cardGamePlayer.HandCard.cards.count..","..toWhere)
            end,(i-1)*0.1)
        end
    end
end)
