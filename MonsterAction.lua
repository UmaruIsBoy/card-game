--成长生命
GrowHealth = 0
--成长伤害
GrowDamage = 0
CurCardGame.OnRoundOver:Register(function(cardGamePlayer,level)
    GrowHealth = GrowHealth + 0.2
    GrowDamage = GrowDamage + 0.3
end)
--怪物行动设计
--   monster.nextArmor 下回合起甲
--   monster.attackCount 本回合攻击次数
--   monster.attack 本回合攻击伤害，设置0且nextArmor为0时会显示未知行动状态栏
--   monster.attackBuff =  CardGameBuff:Create(BuffEnum.Week,1) --下回合附加攻击buff，查看Model文件的BuffEnum
---------------------------------------------------------------------------------------------------------------
--   史莱姆
--   初始状态设置
CardGameMonsters.OnCreate:Register(function(cardGamePlayer,level)
    if level~="1-1" then return end
    -- 添加两个僵尸
    CardGameMonsters:Add(CardGameMonster:Create("史莱姆"))
    CardGameMonsters:Add(CardGameMonster:Create("史莱姆"))
    -- 设置血量
    for i = 1, CardGameMonsters:Count() do
        local monster = CardGameMonsters:GetByIndex(i)
        monster.health = math.floor(math.random(9,12)+GrowHealth)
        monster.maxHealth = monster.health
        -- 怪物模型
        monster.monsterType = Framework.MonsterType.GREEN_WATER
    end
    -- 获得僵尸，属性查看Model的CardGameMonster
    print("史莱姆1,health:".. CardGameMonsters:GetByIndex(1).health)
    print("史莱姆2,health:".. CardGameMonsters:GetByIndex(2).health)
end)
--   每回合行动，
--   roundNum回合数，从1开始
--   level,大关卡名称例如1-1，1-2
CardGameMonsters.OnSetAction:Register(function(cardGamePlayer,cardGameMonster,roundNum,level)
    local monster = cardGameMonster
    --只设置史莱姆的行动
    if monster.name~="史莱姆" then return end
    --设置回合伤害
    monster.attack = math.floor(math.random(3,5)+GrowDamage)
    local value = math.random(0,100)
    --如果上回合添加过护甲，这回合伤害为3
    --if  monster.nextArmor>0 then
    --    monster.attackCount = 3
    --    monster.nextArmor = 0
    --end
    --将上回合将要添加的护甲添加
    if roundNum>1 and monster.nextArmor>0 then
        monster.armor = monster.nextArmor
        monster.nextArmor = 0
        monster.attackCount = 2
    end
    ---- 百分之40下回合增加护甲
    if value < 40 then
        --monster.attackBuff =  CardGameBuff:Create(BuffEnum.Week,1)
        monster.nextArmor = 10
        monster.attack = 0
        print("下回合增加护甲"..monster.nextArmor)
    end
    -- 两回合后如果没杀掉所有伤害增加3点
    if roundNum>2 then
        monster.attack = monster.attack + 3
    end
end)

MonsterConfigs = {
    --名称 = 生命范围 ， 伤害范围
    ["史莱姆"]={"史莱姆",{9,12},{3,6}},
    ["重甲僵尸"] ="重甲僵尸",{{39,41},{6,12}},
    ["僵尸"] = {"僵尸",{10,13},{6,8}}
}

-- 僵尸
CardGameMonsters.OnCreate:Register(function(cardGamePlayer,level)
    if level~="1-2" then return end
    CardGameMonsters:Add(CardGameMonster:Create("僵尸"))
    CardGameMonsters:Add(CardGameMonster:Create("僵尸"))
    CardGameMonsters:Add(CardGameMonster:Create("僵尸"))
    for i = 1, CardGameMonsters:Count() do
        local monster = CardGameMonsters:GetByIndex(i)
        monster.health = math.floor(math.random(10,13)+GrowHealth)
        monster.maxHealth = monster.health
        -- 怪物模型
        monster.monsterType = Framework.MonsterType.NORMAL0
    end
    print("僵尸1,health:".. CardGameMonsters:GetByIndex(1).health)
    print("僵尸2,health:".. CardGameMonsters:GetByIndex(2).health)
    print("僵尸3,health:".. CardGameMonsters:GetByIndex(2).health)
end)

CardGameMonsters.OnSetAction:Register(function(cardGamePlayer,cardGameMonster,roundNum,level)
    local monster = cardGameMonster
    if monster.name~="僵尸" then return end
    monster.attack = math.floor(math.random(2,5)+GrowDamage)
    local value = math.random(0,100)

    --百分之60攻击附带虚弱
    if value < 60 then
        monster.attackBuff =  CardGameBuff:Create(BuffEnum.Week,2)
        -- 有虚弱buff时攻击 -3
        monster.attack = monster.attack - 3
    end
    --50概率下回合对玩家施加buff
    if roundNum>2 then
        monster.attack = monster.attack + 3
    end
end)

--不要修改这个，这个是遗物的效果 "接下来三回合怪物只有1血" 这段代码请永远放在此文件最后
CardGameMonsters.OnCreate:Register(function(cardGamePlayer,level)
    local treasure=cardGamePlayer:GetTreasure(1)
    if treasure and treasure.layer>0 then
        cardGamePlayer:DecreaseTreasureLayer(treasure.id)
        for i = 1, CardGameMonsters:Count() do
            local monster = CardGameMonsters:GetByIndex(i)
            monster.health = 1
        end
    end
end)