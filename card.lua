--|分段射击|46/146|1/0   |   |      | 分段射击|  1  |技能|自身     |1     | 攻击卡牌伤害/2，段数X2 |
Card.OnUse:Register(function(cardGamePlayer,card)
    if cardGamePlayer:GetBuff(BuffEnum.SegmentShoot)  then
        if card.cardType==CardTypeEnum.Attack then
            card.damage =  math.ceil(card.damage/2)
            card.attackCount =  card.attackCount*2
        end
    end
end)
Card.OnSelected:Register(function(cardGamePlayer,card)
    if cardGamePlayer:GetBuff(BuffEnum.SegmentShoot)  then
        if card.cardType==CardTypeEnum.Attack then
            card.damage =  math.ceil(card.damage/2)
            card.attackCount =  card.attackCount*2
        end
    end
end)
--|粉碎|45/144   |1     |    |8/10  |        |     |技能|自身     | 1    | 选择一张牌消耗        |
Card.OnUsed:Register(function(cardGamePlayer,card,entities)
    if card.id==45 or card.id==145 then
        Invoke(function()
            cardGamePlayer:SignalPlus("StartChoiceCard"..1)
            cardGamePlayer.startChoice = true
            cardGamePlayer.HandCard.toWhere = 2
        end,0)
    end
end)
--|调整|44/144    |1    |    |8/10  |        |     |技能|自身     |1      |选择一张牌放到抽牌堆顶部|
Card.OnUsed:Register(function(cardGamePlayer,card,entities)
    if card.id==44 or card.id==144 then
        Invoke(function()
            cardGamePlayer:SignalPlus("StartChoiceCard"..1)
            cardGamePlayer.startChoice = true
            cardGamePlayer.HandCard.toWhere = 1
        end,0)
    end
end)

--|追击|43/143    |1    |8/10|     |          |    |攻击|单体     | 1     |抽一张牌，如果目标有伤口再抽一张|
Card.OnUsed:Register(function(cardGamePlayer,card,entities)
    if card.id==43 or card.id==143 then
        cardGamePlayer.HandCard:AddCard(cardGamePlayer)
        for i, entity in pairs(entities) do
            if entity:GetBuff(BuffEnum.Wounds) then
                cardGamePlayer.HandCard:AddCard(cardGamePlayer)
                break
            end
        end
    end
end)

--|金钱之力|42/142 |3/2  |    |     |          |    |攻击|单体     | 3    |造成金币数量伤害|
Card.OnUse:Register(function(cardGamePlayer,card)
    if card.id==42 or card.id==142 then
        card.damage = cardGamePlayer:GetCoin()
    end
end)
Card.OnSelected:Register(function(cardGamePlayer,curCard)
    if curCard.id==42 or curCard.id==142 then
        curCard.damage = cardGamePlayer:GetCoin()
    end
end)
--|净化| 40/140   |1/0|     |      |         |    |消耗|自身     | 2     |移除自身所有的负面效果|
Card.OnUse:Register(function(cardGamePlayer,card)
    if card.id==40 or card.id==140 then
        cardGamePlayer:ClearBuff(CardGameBuff:Create(BuffEnum.AllDeBuff))
    end
end)

--|印刷机|39/139  |1   |    |      |         |    |消耗|自身     | 2     |复制(1/3)张下一次打出的牌|
Card.OnUse:Register(function(cardGamePlayer,card)
    if  cardGamePlayer.copyCardCount>0 then
        for i = 1, cardGamePlayer.copyCardCount do
            Invoke(function()
                cardGamePlayer.HandCard:AddNewCard(cardGamePlayer,card)
            end,(i-1)*0.1)
        end
        cardGamePlayer.copyCardCount = 0
    end
    if card.id == 39   then
        cardGamePlayer.copyCardCount = cardGamePlayer.copyCardCount + 1
    elseif card.id == 139  then
        cardGamePlayer.copyCardCount = cardGamePlayer.copyCardCount + 3
    end
end)
CurCardGame.OnCardGameInit:Register(function(cardGamePlayer,level)
    cardGamePlayer.copyCardCount = 0
end)
CurCardGame.OnPlayerRoundEnd:Register(function(cardGamePlayer,level)
    cardGamePlayer.copyCardCount = 0
end)
CurCardGame.OnRoundOver:Register(function(cardGamePlayer,level)
    cardGamePlayer.copyCardCount = 0
end)
--|撕裂|38/138    |1   | 3/5  |      | 伤口    | 3/5|攻击| 单体    | 1     |目标被攻击时造成效果层数的伤害|
Card.OnUsed:Register(function(cardGamePlayer,card,entities)
    for entityIndex, cardGameEntity in pairs(entities) do
        local buff = cardGameEntity:GetBuff(BuffEnum.Wounds)
        if buff then
            if card.damage>0 then
                cardGamePlayer:TakeDamage(cardGameEntity, buff.layer)
            end
        end
    end
end)
--|孤注一掷|35/135 |1  | 3   |     |         |    |消耗|单体     |3      |每次拿到手中时伤害增加（6/8）|
Card.OnAddCard:Register(function(cardGamePlayer,card,originCard)
    if card.id == 35   then
        originCard.damage = originCard.damage + 6
    elseif card.id == 135  then
        originCard.damage = originCard.damage + 8
    end
end)

--|专注|34/134    |1/0 |     |     |         |    |消耗|自身      |1      |最大能量加1|
Card.OnUse:Register(function(cardGamePlayer,card)
    if card.id ~= 34 and card.id ~= 134  then
        return
    end
    cardGamePlayer.extraCost = cardGamePlayer.extraCost + 1
end)

--|回收|32/132    |2/1 |0     |    |回收      |1/2  |技能| 自身     |2      |消耗类型卡牌不会消耗|
Card.OnUse:Register(function(cardGamePlayer,card)
    if cardGamePlayer:GetBuff(BuffEnum.Reclaim)  then
        if card.cardType==CardTypeEnum.Deplete then
            card.cardType = CardTypeEnum.Skill
        end
    end
end)
Card.OnSelected:Register(function(cardGamePlayer,card)
    if cardGamePlayer:GetBuff(BuffEnum.Reclaim)  then
        if card.cardType==CardTypeEnum.Deplete then
            card.cardType = CardTypeEnum.Skill
        end
    end
end)
--|盾击|25/125|1/0     |     |    |        |   |攻击  |单体     |1      |造成护盾值伤害|
Card.OnUse:Register(function(cardGamePlayer,card)
    if card.id ~= 25 and card.id ~= 125  then
        return
    end
    card.damage = cardGamePlayer.armor
end)

--|双重释放|27/127|1    |    |     |       |    |技能 | 自身     |2     |本回合下（1/2）张攻击牌攻击段数x2|
Card.OnUse:Register(function(cardGamePlayer,card)
    local user =  cardGamePlayer.user
    if user.doubleAttack and user.doubleAttack>0 and card.cardType == CardTypeEnum.Attack then
        card.attackCount = card.attackCount*2
    end
    if card.id==27 then
        user.doubleAttack = 1
    end
    if card.id==127 then
        user.doubleAttack = 2
    end
end)
Card.OnUsed:Register(function(cardGamePlayer,card,entities)
    local user =  cardGamePlayer.user
    if  user.doubleAttack and user.doubleAttack>0 and card.cardType == CardTypeEnum.Attack then
        user.doubleAttack = user.doubleAttack - 1
    end
end)
CurCardGame.OnPlayerRoundEnd:Register(function(cardGamePlayer)
    cardGamePlayer.user.doubleAttack = 0
end)
--|偷窃| 41/141   |1   |3/5 |      |         |    |攻击|单体     | 2     |偷窃（10~20/20~40）金币   |
Card.OnUsed:Register(function(cardGamePlayer,card,entities)
    if card.id==41 then
        for i, v in pairs(entities) do
            cardGamePlayer:AddCoin(math.random(10,20))
        end
    end
    if card.id==141 then
        for i, v in pairs(entities) do
            cardGamePlayer:AddCoin(math.random(20,40))
        end
    end
end)
--==============如果想让“变为单体”/“变为群体”/“自残”作用于你的卡牌，请把代码放在以下方法上方=================
Card.OnUse:Register(function(cardGamePlayer,card)
    --变为单体（只能给玩家施加）
    if  cardGamePlayer:GetBuff(BuffEnum.ToSingle) then
        if card.cardTarget==CardTargetEnum.Multiple  then
            card.cardTarget = CardTargetEnum.Single
            card.damage = CardGameMonsters:AliveCount()*card.damage
            card.buff.layer = CardGameMonsters:AliveCount()*card.buff.layer
        end
    end
    --变为群体（只能给玩家施加）
    if  cardGamePlayer:GetBuff(BuffEnum.ToMultiple) then
        if card.cardTarget==CardTargetEnum.Single  then
            card.cardTarget = CardTargetEnum.Multiple
            card.damage = math.ceil(card.damage/CardGameMonsters:AliveCount())
            card.buff.layer =  math.ceil(card.buff.layer/CardGameMonsters:AliveCount())
        end
    end
    --|自残
    if  cardGamePlayer:GetBuff(BuffEnum.SelfHarm) then
        if card.cardTarget~=CardTargetEnum.Self  then
            card.cardTarget = CardTargetEnum.Self
        end
    end
end)
-- |巩固|28/128|2/1      |    |    |        |     |技能 | 自身    |2      |护盾翻倍|
Card.OnUse:Register(function(cardGamePlayer,card)
    if card.id == 28 or card.id == 128  then
        local armor = 0
        armor = armor + cardGamePlayer.armor
        if armor>0 then
            cardGamePlayer:AddArmor(armor)
        end
    end
end)
--防御姿态
Card.OnUse:Register(function(cardGamePlayer,card)
    if  cardGamePlayer:GetBuff(BuffEnum.DefensiveStance) then
        if card.damage>0 then
            cardGamePlayer:AddArmor(card.damage)
        end
    end
end)

--涂毒/涂毒+
Card.OnUsed:Register(function(cardGamePlayer,card,entities)
    if card.cardType == CardTypeEnum.Attack then
        local buff
        if  cardGamePlayer:GetBuff(BuffEnum.AttackPoison) then
            buff = CardGameBuff:Create(BuffEnum.Poison,1)
        end
        if  cardGamePlayer:GetBuff(BuffEnum.AttackPoisonPlus) then
            if buff then
                buff.layer = buff.layer + 3
            else
                buff = CardGameBuff:Create(BuffEnum.Poison,3)
            end
        end
        if buff then
            for entityIndex, cardGameEntity in pairs(entities) do
                cardGameEntity:AddBuff(buff)
                cardGamePlayer:SignalPlus("AddMonsterBuff"..buff:ToString()..",".. entityIndex)
            end
        end
    end
end)

--针对防御
Card.OnUsed:Register(function(cardGamePlayer,card,entities)
    if card.id == 17 or card.id == 117  then
        local armor = 0
        for entityIndex, cardGameEntity in pairs(entities) do
            armor = armor + cardGameEntity.attack
        end
        if armor>0 then
            cardGamePlayer:AddArmor(armor)
        end
    end
end)
--抽卡
Card.OnUsed:Register(function(cardGamePlayer,card,entities)
    if card.id ~= 7 and card.id ~= 107  then
        return
    end
    if card.cardTarget == CardTargetEnum.Self then
        local addCardCount = 2
        for i = 1, addCardCount do
            Invoke(function()
                cardGamePlayer.HandCard:AddCard(cardGamePlayer)
            end,(i-1)*0.2)
        end
    end
end)
--折磨
Card.OnUsed:Register(function(cardGamePlayer,card,entities)
    if card.id ~= 21 and card.id ~= 121  then
        return
    end
    for entityIndex, cardGameEntity in pairs(entities) do
        for k,v in pairs(BuffEnum) do
            local buff = cardGameEntity:GetBuff(v)
            if buff then
                if card.id == 121 then
                    buff.layer =  buff.layer*2
                end
                cardGameEntity:AddBuff(buff)
                local monsterBuff = cardGameEntity:GetBuff(buff.id)
                cardGamePlayer:SignalPlus("AddMonsterBuff"..monsterBuff:ToString()..",".. entityIndex)
            end
        end
    end
end)
--  |毒性爆发|22/122|1/0  |0    |     |        |     |技能  |  单体 |1      |立刻造成目标中毒层数伤害|
Card.OnUsed:Register(function(cardGamePlayer,card,entities)
    if card.id ~= 22 and card.id ~= 122  then
        return
    end
    for entityIndex, cardGameEntity in pairs(entities) do
        local buff = cardGameEntity:GetBuff(BuffEnum.Poison)
        if buff then
            cardGamePlayer:TakeDamage(cardGameEntity, buff.layer)
        end
    end
end)
--|自残  |23/123 |-2/-3|     |     |自残   |1     |技能  |自身   |2     |所有卡牌目标变为自身，抽(3/5)张牌|
Card.OnUsed:Register(function(cardGamePlayer,card,entities)
    if card.id ~= 23 and card.id ~= 123  then
        return
    end
    if card.cardTarget == CardTargetEnum.Self then
        local addCardCount = 3
        if card.id == 123  then
            addCardCount = 5
        end
        for i = 1, addCardCount do
            Invoke(function()
                cardGamePlayer.HandCard:AddCard(cardGamePlayer)
            end,(i-1)*0.2)
        end
    end
end)

--|伤害转移|24/124|3/2  |     |    |伤害转移|1   |消耗  |单体     |3     |由效果目标承受伤害，有多个目标时分散伤害|
function GetInjuryTransferEntities(cardGamePlayer)
    local injuryTransferCount = 0
    local injuryTransferEntities = {}
    local entities = CardGameMonsters:GetAliveAll()
    for entityIndex, cardGameEntity in pairs(entities) do
        if cardGameEntity:GetBuff(BuffEnum.Transfer) then
            injuryTransferCount = injuryTransferCount +1
            table.insert(injuryTransferEntities,cardGameEntity)
        end
    end
    if cardGamePlayer:GetBuff(BuffEnum.Transfer) then
        injuryTransferCount = injuryTransferCount +1
        table.insert(injuryTransferEntities,cardGamePlayer)
    end
    return injuryTransferEntities
end

CurCardGame.OnCardGameInit:Register(function(cardGamePlayer,level)
    local entities = CardGameMonsters:GetAliveAll()
    for entityIndex, cardGameEntity in pairs(entities) do
        cardGameEntity.OnBeforeTakeDamage:Register(function(attacker,victim)
            local InjuryTransferEntities = GetInjuryTransferEntities(cardGamePlayer)
            if #InjuryTransferEntities >0 then
                for k,entity in pairs(InjuryTransferEntities) do
                    cardGamePlayer.attack = math.ceil(attacker.attack/#InjuryTransferEntities)
                    cardGamePlayer.attackCount = attacker.attackCount
                    cardGamePlayer.attackBuff = attacker.attackBuff
                    CardGameEntity.TakeDamage(cardGamePlayer,entity)
                end
                return false
            end
            return attacker.attack
        end,REGISTER_MODE.only)
    end
    cardGamePlayer.OnBeforeTakeDamage:Register(function(attacker,victim)
        local InjuryTransferEntities = GetInjuryTransferEntities(cardGamePlayer)
        if #InjuryTransferEntities >0 then
            for k,entity in pairs(InjuryTransferEntities) do
                cardGamePlayer.attack = math.ceil(attacker.attack/#InjuryTransferEntities)
                cardGamePlayer.attackCount = attacker.attackCount
                cardGamePlayer.attackBuff = attacker.attackBuff
                CardGameEntity.TakeDamage(cardGamePlayer,entity)
            end
            return false
        end
        return attacker.attack
    end,REGISTER_MODE.only)
end)
--OnAttacked中已经计算过重伤和虚弱了
CardGameEntity.OnAttacked:Register(function(attacker,victim,damage,buff)
    if victim and attacker then
        --受害者有反甲buff
        local thornMailBuff = victim:GetBuff(BuffEnum.ThornMail)
        if thornMailBuff then
            if attacker~=victim then
                victim:TakeDamage(attacker,thornMailBuff.layer,buff)
            end
        end
    end
end)
--OnAttacked中已经计算过重伤和虚弱了
CardGameEntity.OnAttacked:Register(function(attacker,victim,damage,buff)
    if victim and attacker then
        --攻击者有伤害转换buff
        if attacker:GetBuff(BuffEnum.DamageConservation) then
            if attacker:IsPlayer() and victim:IsMonster() then
                attacker:AddHealth(damage)
            elseif attacker:IsMonster() and victim:IsPlayer() then
                CardGameEntity.Hurt(attacker,-damage)
                victim:SignalPlus("ReturnTargetInfo".. attacker:ToString())
            end
        end
    end
end)
Card.OnSelected:Register(function(cardGamePlayer,curCard)
    if curCard.id ~= 25 and curCard.id ~= 125  then
        return
    end
    curCard.damage = cardGamePlayer.armor
end)
Card.OnSelected:Register(function(cardGamePlayer,curCard)
    if curCard.id ~= 25 and curCard.id ~= 125  then
        return
    end
    curCard.damage = cardGamePlayer.armor
end)

--|双重释放|27/127|1    |    |     |       |    |技能 | 自身     |2     |本回合下（1/2）张攻击牌攻击段数x2|
Card.OnSelected:Register(function(cardGamePlayer,card)
    local user =  cardGamePlayer.user
    if user.doubleAttack and user.doubleAttack>0 and card.cardType == CardTypeEnum.Attack then
        card.attackCount = card.attackCount*2
    end
end)


--如果想让“变为单体”/“变为群体”/“自残”作用于你的卡牌，请把代码放在以下方法上方
--Card.OnSelected无法更改卡牌实际数值，只能影响显示
--在Card.OnUse:Register或者Card.OnUsed:Register中更改实际卡牌的数值
Card.OnSelected:Register(function(cardGamePlayer, card)
    --变为单体（只能给玩家施加）
    if  cardGamePlayer:GetBuff(BuffEnum.ToSingle) then
        if card.cardTarget==CardTargetEnum.Multiple  then
            card.cardTarget = CardTargetEnum.Single
            card.damage = CardGameMonsters:AliveCount()* card.damage
            card.buff.layer = CardGameMonsters:AliveCount()* card.buff.layer
        end
    end
    --变为群体（只能给玩家施加）
    if  cardGamePlayer:GetBuff(BuffEnum.ToMultiple) then
        if card.cardTarget==CardTargetEnum.Single  then
            card.cardTarget = CardTargetEnum.Multiple
            card.damage = math.floor(card.damage/CardGameMonsters:AliveCount())
            card.buff.layer =  math.floor(card.buff.layer/CardGameMonsters:AliveCount())
        end
    end
    --|自残
    if  cardGamePlayer:GetBuff(BuffEnum.SelfHarm) then
        if card.cardTarget~=CardTargetEnum.Self  then
            card.cardTarget = CardTargetEnum.Self
        end
    end
end)