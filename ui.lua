--Q抽牌堆,E弃牌堆
ScreenCenter={x = UI.ScreenSize().width/2,y = UI.ScreenSize().height/2}
CardGameState=UI.SyncValue.Create("CardGameState"..UI.PlayerIndex())
PlayerCost = 0
PlayerMaxCost = 3


CardsColor={
    [1]={r=255,g=102,b=102,a=255},--#FF6666 攻击
    [2]={r=92,g=204,b=204,a=255},--#5CCCCC 技能
    [3]={r=163,g=218,b=231,a=255},--# 消耗
}

--Control
Framework.KeyDown.LEFT:Register(function()
    if CardGameState.value~=GameStateEnum.PlayerAttackState then return end
    UI.SignalPlus("Control".. ControlEnum.Left)
end)
Framework.KeyDown.A:Register(function()
    if CardGameState.value~=GameStateEnum.PlayerAttackState then return end
    UI.SignalPlus("Control".. ControlEnum.Left)
end)
Framework.KeyDown.RIGHT:Register(function()
    if CardGameState.value~=GameStateEnum.PlayerAttackState then return end
    UI.SignalPlus("Control".. ControlEnum.Right)
end)
Framework.KeyDown.D:Register(function()
    if CardGameState.value~=GameStateEnum.PlayerAttackState then return end
    UI.SignalPlus("Control".. ControlEnum.Right)
end)
Framework.KeyDown.UP:Register(function()
    Up()
end)
Framework.KeyDown.W:Register(function()
    Up()
end)
Framework.KeyDown.S:Register(function()
    UnSelect()
end)
Framework.KeyDown.DOWN:Register(function()
    UnSelect()
end)
Framework.KeyDown.SPACE:Register(function()
    if CardGameState.value~=GameStateEnum.PlayerAttackState then return end
    if CardsChoice.startChoice then
        Player:SetRoundEndText("空格结束回合")
        CardsChoice:EndChoice()
    else
        UI.SignalPlus("RoundEnd")
        Player:RoundEnd()
    end
end)
Framework.KeyDown.MOUSE1:Register(function()
    if CardGameState.value~=GameStateEnum.PlayerAttackState then return end
    if Cards.cardNum==0 then return end
    if Cards.curHandCardIndex==0 then return end
    if Cards.hasTarget then
        UI.SignalPlus("GetUseCard")
    else
        UI.SignalPlus("GetTargetIndex")
    end
end)
Framework.KeyDown.MOUSE2:Register(function()
    if CardGameState.value~=GameStateEnum.PlayerAttackState then return end
    Cards:UnSelectTarget()
    Targets:UnSelected()
    UI.SignalPlus("HasTarget"..tostring(Cards.hasTarget))
end)

function UnSelect()
    if CardGameState.value~=GameStateEnum.PlayerAttackState then return end
    Cards:UnSelectTarget()
    Targets:UnSelected()
    UI.SignalPlus("HasTarget"..tostring(Cards.hasTarget))
end
function Up()
    if CardGameState.value~=GameStateEnum.PlayerAttackState then return end
    if Cards.cardNum==0 then return end
    if Cards.curHandCardIndex==0 then return end
    if Cards.hasTarget then
        --UI.SignalPlus("GetUseCard")
    else
        UI.SignalPlus("GetTargetIndex")
    end
end
--Sync

function CardGameState:OnSync()
    if self.value==GameStateEnum.Default then
        print("=========Default========")
        UI.StopPlayerControl(true)
        Cards=CardsUI:Create(UI.ScreenSize())
        Targets = TargetsUI:Create(UI.ScreenSize())
        Player = PlayerUI:Create(UI.ScreenSize())
        Player:SetCostUI(PlayerCost,PlayerMaxCost)
        UI.SignalPlus("GetTargetsCount")
        UI.SignalPlus("GetTargetsInfo")
    end
    if self.value==GameStateEnum.RoundStartState then
        print("========OnRoundStartUI========")
        Targets:RemoveBuffLayer()
        Player:RemoveBuffLayer()
        UI.SignalPlus("GetHandCardCount")
        UI.SignalPlus("GetTargetsInfo")
        Targets:RoundStart()
    end
    if self.value==GameStateEnum.PlayerAttackState then

    end
    if self.value==GameStateEnum.PlayerRoundEndState then
        Cards:OnRoundEnd()
        Cards:HideDesc()
        Targets:UnSelected()
    end
    if self.value==GameStateEnum.MonsterAttackState then

    end
    if self.value==GameStateEnum.MonsterRoundEndState then
    end
    if self.value==GameStateEnum.RoundOverState then
        print("=========OnRoundOverUI========")
        Cards:OnRoundEnd()
        Cards:HideDesc()
        Player:Hide()
        Targets:OnRoundEnd()
        UI.StopPlayerControl(false)
    end
end



Framework.UIPlug.OnSignalPlus:Register(function(msg)
    local outArgs={}
    if Framework.GetSignalPlusArgs(msg,"PlayerCost",outArgs) then
        local args = outArgs.numArgs
        PlayerCost = args[1]
        PlayerMaxCost = args[2]
        Player:SetCostUI(PlayerCost,PlayerMaxCost)
    end
    --回合开始时执行一次
    if Framework.GetSignalPlusArgs(msg,"ReturnTargetsCount",outArgs)   then
        local targetCount=outArgs.numArgs[1]
        Targets:SetPos(targetCount)
        Targets:Init()
    end
    if Framework.GetSignalPlusArgs(msg,"ReturnTargetInfo",outArgs) then
        local args = outArgs.strArgs
        print("ReturnTargetInfo")
        Targets:SetMonsters(args,tonumber(args[#args]),Player)
    end
    if Framework.GetSignalPlusArgs(msg,"AddMonsterBuff",outArgs)  then
        local args = outArgs.strArgs
        Targets:SetBuff(args,tonumber(args[#args]))
    end
    if Framework.GetSignalPlusArgs(msg,"AddPlayerBuff",outArgs) then
        local args = outArgs.strArgs
        Player:SetBuff(args)
    end
    if Framework.GetSignalPlusArgs(msg,"ClearPlayerBuff",outArgs) then
        local args = outArgs.numArgs
        Player:ClearBuff(args[1])
    end
    if msg=="PlayerAttacked" then
        Player:Attacked()
    end
    if msg=="PlayerRecovery" then
        Player:Recovery()
    end
    if msg=="PlayerAddArmor" then
        Player:AddArmor()
    end
    if Framework.GetSignalPlusArgs(msg,"ReturnCurCard",outArgs) then
        local args = outArgs.strArgs
        local card=Card:Create(args)
        local curCardIndex = tonumber(args[#args])
        Cards:SetCard(card,curCardIndex)
        if curCardIndex==0 then
            return
        end
        Cards:SelectCard(curCardIndex)
        Cards:ShowDesc(card,Targets,nil,Player)
    end
    if Framework.GetSignalPlusArgs(msg,"ReturnHandCardCount",outArgs) then
        local handCardCount = outArgs.numArgs[1]
        Cards:SetPos(handCardCount)
        Invoke(function()
            Cards:UpdatePos()
        end)
    end
    if msg=="SelectCard" then
        UI.SignalPlus("GetCurCard")
    end
    if msg=="SelectTarget" then
        UI.SignalPlus("GetTargetIndex")
    end
    if Framework.GetSignalPlusArgs(msg,"AddCard",outArgs) then
        local args = outArgs.strArgs
        local card=Card:Create(args)
        Cards:AddCard(card,tonumber(args[#args]))
    end
    if Framework.GetSignalPlusArgs(msg,"ReturnUseCard",outArgs) then
        local args = outArgs.numArgs
        --使用的手牌id
        local useCardIndex = args[1]
        --使用后的手牌数
        local handCardCount = args[2]

        local toWhere = args[3]

        if useCardIndex~=0 then
            Cards:UseCard(useCardIndex,toWhere)
        else
            --某些原因导致返回0，比如费用不够
            Cards:UnSelectTarget()
            Targets:UnSelected()
        end
        Cards:SetPos(handCardCount)
        Invoke(function()
            Cards:UpdatePos()
            UI.SignalPlus("GetCurCard")
        end)
        UI.SignalPlus("HasTarget"..tostring(Cards.hasTarget))
    end
    if Framework.GetSignalPlusArgs(msg,"ReturnTargetIndex",outArgs) then
        local args = outArgs.strArgs
        local card=Card:Create(args)
        local curCardIndex = tonumber(args[#args-1])
        local targetIndex = tonumber(args[#args])
        print("ReturnTargetIndex"..targetIndex)
        Cards:SetCard(card,curCardIndex)
        Cards:SelectTarget(Targets,targetIndex,Player)
        Cards:ShowDesc(card,Targets,targetIndex,Player)
        UI.SignalPlus("HasTarget"..tostring(Cards.hasTarget))
    end
    if Framework.GetSignalPlusArgs(msg,"ShowDamage",outArgs) then
        local args = outArgs.numArgs
        Targets:SetDamageText(args[1],args[2])
    end

    --部分卡牌作用可以选取
    if Framework.GetSignalPlusArgs(msg,"StartChoiceCard",outArgs) then
        local args = outArgs.numArgs
        local choiceMaxNum = args[1]
        for i = 1, choiceMaxNum do
            CardsChoice.emptyState[i] = true
        end
        CardsChoice.startChoice = true
        Player:SetRoundEndText("空格选择完毕")
        CardsChoice:SetPos(choiceMaxNum)
    end
    if Framework.GetSignalPlusArgs(msg,"ChoiceCard",outArgs) then
        local args = outArgs.numArgs
        local cardIndex = args[1]
        local cardUI=Cards:GetCardUI(cardIndex)
        CardsChoice:AddCard(cardUI,cardIndex)
    end
end)