function SelectRandomElementInList(list,randomNum)
    if randomNum>#list then
        randomNum = #list
    end
    local tableIndex={}
    for i = 1, #list do
        tableIndex[i] = i
    end
    local randomIndex={}
    for i = 1, randomNum do
        local index=math.random(1,#tableIndex)
        table.insert(randomIndex,tableIndex[index])
        table.remove(tableIndex,index)
    end
    local rst={}
    for i = 1, #randomIndex do
        table.insert(rst,list[randomIndex[i]])
    end
    return rst
end
function RandomLevelUpCard(cardList)
    for i = 1, #cardList do
        local value = math.random(0,100)
        -- 百分之10为升级的卡
        if value<10 then
            cardList[i] = cardList[i] + 100
        end
    end
end
