--地图中的类

--用于传送的关键词
EventTP ={
    --事件开始坐标，结束坐标
    [1]={{x=-1,y=-15,z=1},{x=0,y=-43,z=2}},
    [2]={{x=-70,y=-66,z=1},{x=0,y=-66,z=2}}
}
GameMonsterPos={
    ["1-1"]={{x=-36,y=-64,z=6},{x=-47,y=-64,z=6}},
    ["1-2"]={{x=-35,y=-102,z=6},{x=-41,y=-102,z=6},{x=-47,y=-102,z=6}}
}
GamePlayerPos={
    ["1-1"]={{x=-41,y=-49,z=6},{x=-1,y=-54,z=4}},
    ["1-2"]={{x=-41,y=-88,z=6},{x=-1,y=-42,z=3}}
}
GameMonsterDeathPos={x=23,y=-52,z=3}

if Game then
    --关卡开始和结束的传送
    CurCardGame.OnCardGameInit:Register(function(cardGamePlayer,level)
        cardGamePlayer.player.position = GamePlayerPos[level][1]
    end)
    CurCardGame.OnRoundOver:Register(function(cardGamePlayer,level)
        if cardGamePlayer.death then
            cardGamePlayer.player:Respawn()
            return
        end
        cardGamePlayer.player.position = GamePlayerPos[level][2]
    end)

    CardGameMonsters.OnCreate:Register(function(cardGamePlayer,level)
        GameMonsters={}
        for i = 1, #GameMonsterPos[level] do
            local pos = GameMonsterPos[level][i]
            local cardGameMonster = CardGameMonsters:GetByIndex(i)
            local monster=Game.Monster:Create(cardGameMonster.monsterType,pos)
            monster.viewDistance = 1
            table.insert(GameMonsters,monster)
            local newPos = {x=pos.x,y=pos.y + 1,z=pos.z}
            monster:MoveTo(newPos)
            Invoke(function()
                monster:Stop(true)
            end,1)
            cardGameMonster.OnDeath:Register(function()
                print("=========OnDeath=============")
                GameMonsters[i].position = GameMonsterDeathPos
                Invoke(function(index)
                    GameMonsters[index].origin.position = pos
                end,0.5,i)
            end)
            cardGameMonster.OnAttacked:Register(function(cardGamePlayer, damage, buff)
                monster:ShowOverheadDamage(damage,cardGamePlayer.player.index)
            end)
            cardGameMonster.OnTakeDamageAni:Register(function()
                if not cardGameMonster.death then
                    monster.velocity={x=0,y=0,z=300}
                end
            end)
        end
    end)


    --通过脚本调用方块传入RoundStart参数Event,{eventName},{level}
   function CardGameEvent.OnStart(cardGamePlayer,eventId,eventPosId)
       --事件已经开始
        cardGamePlayer:SignalPlus("EventStart"..eventId)
        cardGamePlayer.player.maxspeed = 0.001
        cardGamePlayer.player.velocity = {x=0,y=0,z=0}
        cardGamePlayer.eventId = eventId
        cardGamePlayer.eventPosId = eventPosId
        cardGamePlayer.player.position = EventTP[eventPosId][1]
        --不需要传入ui的事件
        if eventId==1 then
            cardGamePlayer:AddTreasure(CardGameTreasure:Create(1))
            CardGameEvent.OnEnd(cardGamePlayer,eventId)
        end
    end

    function CardGameEvent.OnEnd(cardGamePlayer,eventId)
        if cardGamePlayer.eventId == nil then return end
        if eventId == cardGamePlayer.eventId  then
            cardGamePlayer.player.position = EventTP[cardGamePlayer.eventPosId][2]
            cardGamePlayer.curEventName  = nil
            cardGamePlayer.eventId = nil
            cardGamePlayer.eventPosId = nil
            cardGamePlayer.player.maxspeed = 1
        end
    end
    Framework.GamePlug.OnPlayerSignalPlus:Register(function(player,msg)
        local outArgs={}
        local cardGamePlayer = Players[player.index]
        if Framework.GetSignalPlusArgs(msg,"EventStart",outArgs)  then
            local eventId = outArgs.numArgs[1]
            CardGameEvent.OnStart(cardGamePlayer,eventId,cardGamePlayer.eventPosId)
        end
        if msg=="GetKnapsackCards" then
            cardGamePlayer:SignalPlus("ReturnCardKnapsackCards"..cardGamePlayer.CardKnapsack:ToString()..","..cardGamePlayer.CardKnapsack.toStringArgsCount)
        end

        --获得未升级的卡牌
        if msg=="GetUnLevelUpKnapsackCards" then
            cardGamePlayer:SignalPlus("ReturnCardKnapsackCards"..cardGamePlayer.CardKnapsack:UnLevelUpToString()..","..cardGamePlayer.CardKnapsack.toStringArgsCount)
        elseif Framework.GetSignalPlusArgs(msg,"EventEnd",outArgs) then
            local eventId=outArgs.numArgs[1]
            if eventId==3 then
                local levelUpIndex=outArgs.numArgs[2]
                cardGamePlayer.CardKnapsack:LevelUpCard(levelUpIndex)
            end
            if eventId==2 then
                local removeIndex=outArgs.numArgs[2]
                cardGamePlayer.CardKnapsack:RemoveCard(removeIndex)
            end
            if eventId==5 then

            end
            CardGameEvent.OnEnd(cardGamePlayer,eventId)
        end
    end)
end
if UI then
    Framework.UIPlug.OnSignalPlus:Register(function(msg)
        local outArgs={}
        if Framework.GetSignalPlusArgs(msg,"EventStart",outArgs) then
            local eventId=outArgs.numArgs[1]
            if eventId==2 then
                CardKnapsackUI:Show(CardKnapsackUI.modeEnum.RemoveCard)
            end
            if eventId==3 then
                CardKnapsackUI:Show(CardKnapsackUI.modeEnum.LevelUpCard)
            end
            if eventId==4 then
                StartEvent =  Draw.MenuStrip:Create(ScreenCenter)
                StartEvent:Add(CardGameEvent[1][2])
                StartEvent:Add(CardGameEvent[2][2])
                StartEvent:Add(CardGameEvent[3][2])
                StartEvent.OnSelect:Register(function(menu, focusIndex)
                    local eventName = StartEvent:GetName(StartEvent:Get().focusIndex)
                    local eventIndex
                    for i = 1, #CardGameEvent do
                        if eventName == CardGameEvent[i][2] then
                            eventIndex = CardGameEvent[i][1]
                        end
                    end
                    if eventIndex then
                        UI.SignalPlus("EventStart".. eventIndex)
                    end
                    StartEvent:Hide()
                end)
                StartEvent:Show()
                if not StartEvent.hasAdaptation then
                    StartEvent.hasAdaptation = true
                    StartEvent:Adaptation("center")
                end
                StartEvent:OnFocus(1)
            end
            --商店
            if eventId==5 then
                UI.SignalPlus("GetShopCardsInfo")
            end
        end

        if Framework.GetSignalPlusArgs(msg,"ReturnCardKnapsackCards",outArgs)  then
            CardKnapsackUI:ShowBG()
            CardKnapsackUI:SetCards(outArgs.numArgs)
        end
    end)

    CardKnapsackUI={
        spanX=10,
        spanY=10,
        mode = nil,
        headHeight = 120,
        tailHeight = 30,
        modeEnum={
            RemoveCard =1,
            LevelUpCard =2,
            ShowAllCard = 3,
        },
        RemoveCardIsVisible = false,
        LevelUpCardIsVisible = false,
        ShowAllCardIsVisible = false,
        OnSelectCard = REGISTER:New()
    }

    function CardKnapsackUI:Show(mode,sender)
        self.sender = sender or self.sender
        CardKnapsackUI.mode = mode
        if CardKnapsackUI.mode==CardKnapsackUI.modeEnum.RemoveCard then
            self.RemoveCardIsVisible = true
            UI.SignalPlus("GetKnapsackCards")
        end
        if CardKnapsackUI.mode==CardKnapsackUI.modeEnum.LevelUpCard then
            self.LevelUpCardIsVisible = true
            UI.SignalPlus("GetUnLevelUpKnapsackCards")
        end
        if CardKnapsackUI.mode==CardKnapsackUI.modeEnum.ShowAllCard then
            self.ShowAllCardIsVisible = true
            UI.SignalPlus("GetKnapsackCards")
        end
        UI.StopPlayerControl(true)
    end
    function CardKnapsackUI:GetSelectData()
        local row = self.row
        local col = self.col
        row = row + (self.page-1)*3
        if not self.cardsUISet[row] or not self.cardsUISet[row][col] then return end
        return self.cardsUISet[row][col]
    end
    function CardKnapsackUI:ShowBG()
        self.IsVisible = true
        self.BG = self.BG or UI.Box:Create()
        self.SelectBG = self.SelectBG or UI.Box:Create()
        self.pageText = UI.Text:Create()
        self.BG:Set(ScreenCenter)
        --5张卡宽+6个10像素间隔
        --3张卡高+4个10像素间隔
        self.BG:Set({width=CardsUI.cardWidth*5+self.spanX*6,height=self.headHeight+self.tailHeight+CardsUI.cardHeight*3+self.spanY*4,r=105,g=105,b=105,a=200})
        self.BG:Adaptation("center")
        local bg =self.BG:Get()
        self.pageText:Set({text="1/1",align="center",font="medium",width= bg.width,height=self.tailHeight,r=255,g=255,b=255,x=bg.x,y=bg.y+bg.height-self.tailHeight})
        self.cardsUISet = {}
        --只存15个卡牌的ui
        self.cardsUI = {}
        self.page = 1
        self.maxPage = 1
    end
    --CardKnapsack:ToString()
    function CardKnapsackUI:SetCards(args)
        --最后一个参数是参数个数
        local cardArgsCount = args[#args]
        local argsCount = #args-1
        local cardCount = argsCount/ cardArgsCount
        self.cardCount = cardCount
        --每行5张牌
        if math.floor(cardCount/5)>=cardCount/5 then
            self.line = math.floor(cardCount/5)
        else
            self.line = math.floor(cardCount/5) + 1
        end

        if math.floor(cardCount/15)>=cardCount/15 then
            self.maxPage = math.floor(cardCount/15)
        else
            self.maxPage = math.floor(cardCount/15) + 1
        end

        for i = 1, self.line do
            self.cardsUISet[i]={}
        end
        local row = 1
        local col = 1
        local index = 1
        local rowCount = 1
        for i = 1, argsCount, cardArgsCount do
            local card = {
                id = args[i],
                cost = args[i+1],
                damage = args[i+2],
                armor = args[i+3],
                cardType = args[i+4],
                attackCount = args[i+5],
            }

            local bgSet= self.BG:Get()
            local pos = { x=self.spanX+bgSet.x+ (col-1) *(CardsUI.cardWidth+self.spanX), y=self.headHeight+self.spanY+bgSet.y+(row-1)*(CardsUI.cardHeight+self.spanY)}
            if index<=15 then
                local ui=CardsUI:CreateCardUI()
                CardsUI:SetCardUI(ui,card,pos)
                table.insert(self.cardsUI,ui)
            end
            table.insert(self.cardsUISet[rowCount], { pos=pos, index=index, card=card})
            index = index + 1
            col = col + 1
            if col > 5 then
                col = 1
                row = row + 1
                rowCount = rowCount + 1
            end
            if row > 3 then
                row = 1
            end
        end
        self.startSelect = true
        if cardCount>0 then
            self:SetSelectBG(1,1)
        end
        collectgarbage()
    end
    function CardKnapsackUI:GetMaxCol()
        return #self.cardsUISet[self.row]
    end
    function CardKnapsackUI:GetMaxRow()
        if self.page==self.maxPage then
            if self.cardsUISet[self.row] and not self.cardsUISet[self.row][self.col]  then
                return #self.cardsUISet[self.row] - 1
            end
            return self.line - (self.maxPage-1)*3
        else
            return 3
        end
    end

    function CardKnapsackUI:SelectCard()
        if self.mode == CardKnapsackUI.modeEnum.ShowAllCard then return end
        local selectIndex = 0
        if self.cardCount~=0 then
            if not self:GetSelectData() then return end
            local data = self:GetSelectData()
            local startPos =data.pos
            local endPos = {x= ScreenCenter.x-CardsUI.cardWidth/2,y= ScreenCenter.y-CardsUI.cardHeight/2}
            local card = data.card
            if self.mode == CardKnapsackUI.modeEnum.LevelUpCard then
                card = Card:Create(CardsConfig[card.id+100])
            end
            self.selectUI =CardsUI:CreateCardUI()
            CardsUI:SetCardUI(self.selectUI,card,startPos)
            CardsUI:DoMove(self.selectUI,startPos,endPos,2)
            selectIndex = data.index
        end
        if self.mode == CardKnapsackUI.modeEnum.RemoveCard then
            CardsUI:DoColorAlpha(self.selectUI,3)
            UI.SignalPlus("EventEnd2,".. selectIndex)
        end
        if self.mode == CardKnapsackUI.modeEnum.LevelUpCard then
            UI.SignalPlus("EventEnd3,".. selectIndex)
        end
        Invoke(function()
            CardKnapsackUI.selectUI =nil
            collectgarbage()
        end,3, selectIndex)
        CardKnapsackUI.OnSelectCard:Trigger()
        self.startSelect = false
        self:Hide()
    end
    function CardKnapsackUI:Hide()
        if self.mode==self.modeEnum.RemoveCard then
            self.RemoveCardIsVisible = false
        end
        if self.mode==self.modeEnum.LevelUpCard then
            self.LevelUpCardIsVisible = false
            self.startSelect = false
        end
        if self.mode==self.modeEnum.ShowAllCard then
            self.ShowAllCardIsVisible = false
        end

        if self.RemoveCardIsVisible==false and self.LevelUpCardIsVisible==false and self.ShowAllCardIsVisible==false then
            self.cardsUISet = nil
            self.cardsUI = nil
            self.BG = nil
            self.pageText = nil
        end

        self.startSelect = false
        self.SelectBG = nil
        self.desc = nil
        self.desc2 = nil
        collectgarbage()
        UI.StopPlayerControl(false)
        if self.sender then
            Invoke(function(sender)
                sender:Show()
            end,0.1,self.sender)
            collectgarbage()
        else
            if self.LevelUpCardIsVisible == true then
                self:Show(self.modeEnum.LevelUpCard)
            end
            if self.RemoveCardIsVisible == true then
                self:Show(self.modeEnum.RemoveCard)
            end
        end
    end
    function CardKnapsackUI:AddPage()
        self.page = self.page + 1
        if self.page> self.maxPage then
            self.page = 1
        end
        self:UpdateCards()
        self.pageText:Set({text=self.page.."/"..self.maxPage})
    end
    function CardKnapsackUI:DecreasePage()
        self.page = self.page - 1
        if self.page<1 then
            self.page = self.maxPage
        end
        self:UpdateCards()
        self.pageText:Set({text=self.page.."/"..self.maxPage})
    end
    function CardKnapsackUI:GetCardUI(row,col)
        return self.cardsUI[(row-1)*5+col]
    end
    function CardKnapsackUI:UpdateCards()
        --一页有3行
        local uiIndex = 1
        local rowIndex =  (self.page-1)*3+1
        for row = rowIndex, rowIndex+2 do
            --一行6个
            for col = 1, 5 do
                local ui=self.cardsUI[uiIndex]
                if ui then
                    if self.cardsUISet[row] and self.cardsUISet[row][col] then
                        local set = self.cardsUISet[row][col]
                        CardsUI:SetCardUI(ui,set.card,set.pos)
                    else
                        CardsUI:Hide(ui)
                    end
                end
                uiIndex = uiIndex + 1
            end
        end
    end
    function CardKnapsackUI:SetSelectBG(row,col)
        if self.cardCount==0 then
            return true
        end
        self.row = row or self.row
        self.col = col or self.col

        if  self.cardsUISet[self.row] and self.cardsUISet[self.row][self.col ] then
            local pos = self.cardsUISet[self.row][self.col ].pos
            local set={}
            set.x= pos.x-4
            set.y= pos.y-4
            set.width=CardsUI.cardWidth+8
            set.height=CardsUI.cardHeight+8
            --使用怪物被选中的颜色
            local color = TargetsUI.color[3]
            set.r = color.r
            set.g = color.g
            set.b = color.b
            self.SelectBG:Set(set)
        end
        local data = self:GetSelectData()
        local text = ""
        if self.mode ==  CardKnapsackUI.modeEnum.RemoveCard then
            text = "移除"
        end
        if self.mode ==  CardKnapsackUI.modeEnum.LevelUpCard then
            text = "升级"
        end

        if data then
            if self.mode ==  CardKnapsackUI.modeEnum.RemoveCard or self.mode == CardKnapsackUI.modeEnum.ShowAllCard then
                local card = data.card
                local desc2
                if card.damage~=0 then
                    if  card.attackCount==1 then
                        desc2 = string.format(CardsDesc[card.id][2],card.damage)
                    else
                        desc2 = string.format(CardsDesc[card.id][2],card.damage,card.attackCount)
                    end
                elseif card.armor>0 then
                    desc2 = string.format(CardsDesc[card.id][2],card.armor)
                else
                    desc2 = CardsDesc[card.id][2]
                end

                self.desc = DrawText:Create()
                self.desc2 = DrawText:Create()
                print(CardType[card.cardType])
                print(CardsDesc[card.id][1])
                self.desc:Set({text =text..CardType[card.cardType].."："..CardsDesc[card.id][1],x=ScreenCenter.x,y=self.BG:Get().y+20,size=3,kerning=3})
                self.desc2:Set({text =desc2,x=ScreenCenter.x,y=self.BG:Get().y+75,size=2,kerning=4})
                self.desc:Adaptation("upCenter")
                self.desc2:Adaptation("center")
            end
            if self.mode ==  CardKnapsackUI.modeEnum.LevelUpCard then
                self:UpdateCards()
                local card = Card:Create(CardsConfig[data.card.id+100])
                local desc2
                if card.damage~=0 then
                    if card.attackCount==1 then
                        desc2 = string.format(CardsDesc[card.id][2],card.damage)
                    else
                        desc2 = string.format(CardsDesc[card.id][2],card.damage,card.attackCount)
                    end
                elseif card.armor>0 then
                    desc2 = string.format(CardsDesc[card.id][2],card.armor)
                else
                    desc2 = CardsDesc[card.id][2]
                end
                self.desc = DrawText:Create()
                self.desc2 = DrawText:Create()
                self.desc:Set({text =text..CardType[card.cardType].."为："..CardsDesc[card.id][1],x=ScreenCenter.x,y=self.BG:Get().y+20,size=3,kerning=3})
                self.desc2:Set({text =desc2,x=ScreenCenter.x,y=self.BG:Get().y+75,size=2,kerning=4})
                self.desc:Adaptation("upCenter")
                self.desc2:Adaptation("center")
                local ui = self:GetCardUI(self.row,self.col)
                CardsUI:SetCardUI(ui,card,data.pos)
            end
        else
            self.desc = DrawText:Create()
            self.desc:Set({text ="选择"..text.."一张牌",x=ScreenCenter.x,y=self.BG:Get().y+20,size=3,kerning=3})
            self.desc:Adaptation("upCenter")
            self.updateCard = nil
            return false
        end
        return true
    end
    Framework.KeyDown.A:Register(function()
        if   not CardKnapsackUI.startSelect  then return end
        local col =  CardKnapsackUI.col
        col = col -1
        if col< 1 then
            col = 5
            CardKnapsackUI:DecreasePage()
        end
        CardKnapsackUI.col = col
        if not CardKnapsackUI:SetSelectBG() then
            Framework.KeyDown.D:Trigger()
        end
    end)
    Framework.KeyDown.D:Register(function()
        if   not CardKnapsackUI.startSelect  then return end
        local col =  CardKnapsackUI.col
        col = col + 1
        if col > 5 then
            col = 1
            CardKnapsackUI:AddPage()
        end
        CardKnapsackUI.col = col
        if not CardKnapsackUI:SetSelectBG() then
            Framework.KeyDown.A:Trigger()
        end
    end)
    Framework.KeyDown.W:Register(function()
        if   not CardKnapsackUI.startSelect  then return end
        local row =  CardKnapsackUI.row
        row = row - 1
        if row<1 then
            CardKnapsackUI.row = 3
        else
            CardKnapsackUI.row = row
        end
        if not CardKnapsackUI:SetSelectBG() then
            Framework.KeyDown.S:Trigger()
        end
    end)
    Framework.KeyDown.S:Register(function()
        if   not CardKnapsackUI.startSelect  then return end
        local row =  CardKnapsackUI.row
        row = row + 1
        if row>3 then
            CardKnapsackUI.row = 1
        else
            CardKnapsackUI.row = row
        end
        if not CardKnapsackUI:SetSelectBG() then
            Framework.KeyDown.W:Trigger()
        end
    end)

    Framework.KeyDown.MOUSE1:Register(function()
        if CardKnapsackUI.mode == CardKnapsackUI.modeEnum.ShowAllCard then return end
        local knapsack = CardKnapsackUI
        if  knapsack.startSelect  then
            knapsack:SelectCard()
        end
    end)
    Framework.KeyDown.MOUSE2:Register(function()
        --UI.SignalPlus("HasTarget"..tostring(Cards.hasTarget))
    end)
    Framework.KeyDown.Q:Register(function()
        --在商店打开移除卡牌界面的情况
        if CardGameShop.IsVisible  then
            if CardKnapsackUI.RemoveCardIsVisible==false then
                CardGameShop:Hide()
                CardKnapsackUI:Show(CardKnapsackUI.modeEnum.ShowAllCard,CardGameShop)
            else
                CardKnapsackUI:Hide()
            end
        --普通打开查看背包
        elseif not CardKnapsackUI.sender then
            if CardKnapsackUI.ShowAllCardIsVisible==false then
                CardKnapsackUI:Show(CardKnapsackUI.modeEnum.ShowAllCard)
            else
                CardKnapsackUI:Hide()
            end
        else
            CardKnapsackUI:Hide()
        end
    end)
end

--回合结束后选择添加卡牌
if Game then
    SelectAndAddCard={
        ["1-1"]={},
        ["1-2"]={}
    }
    for i = 4, 46 do
        table.insert(SelectAndAddCard["1-1"],i)
        table.insert(SelectAndAddCard["1-2"],i)
    end
    function SelectAndAddCard:GetLevelCards(level)
        local cardsId={}
        for i = 1, #self[level] do
            cardsId[i] = self[level][i]
        end
        return cardsId
    end
    function SelectAndAddCard.GetRandomCardsId(cardGamePlayer,level)
        local cardsId=SelectAndAddCard:GetLevelCards(level)
        RandomLevelUpCard(cardsId)
        return  SelectRandomElementInList(cardsId,3)
    end

    CurCardGame.OnRoundOver:Register(function(cardGamePlayer,level)
        if cardGamePlayer.death then
            return
        end
        print("=========SelectAndAddCard.GetRandomCardsId========")
        local cardsId=SelectAndAddCard.GetRandomCardsId(cardGamePlayer,level)
        local cardsIdStr = table.concat(cardsId,",")
        cardGamePlayer:SignalPlus("SelectAndAddCard"..cardsIdStr)
    end)
    Framework.GamePlug.OnPlayerSignalPlus:Register(function(player,msg)
        local outArgs={}
        if Framework.GetSignalPlusArgs(msg,"ReturnSelectAndAddCard",outArgs) then
            local cardGamePlayer =  Players[player.index]
            if #outArgs.numArgs>0 then
                local addCardIndex = outArgs.numArgs[1]
                cardGamePlayer.CardKnapsack:AddCard(Card:Create(CardsConfig[addCardIndex]))
                --cardGamePlayer:SignalPlus("ReturnCardKnapsackCards"..cardGamePlayer.CardKnapsack:ToString())
            else
                print("跳过选牌")
            end
            cardGamePlayer.player.maxspeed = 1
        end
    end)
end
if UI then
    Framework.UIPlug.OnSignalPlus:Register(function(msg)
        local outArgs={}
        if Framework.GetSignalPlusArgs(msg,"SelectAndAddCard",outArgs) then
            local cardsId = outArgs.numArgs
            CardsUI.SetPos(SelectAndAddCard,#cardsId)
            SelectAndAddCard:CreateCardUI(cardsId)
        end
    end)
    SelectAndAddCard={
        cardWidth = CardsUI.cardWidth,
        startY = 100,
        cardSpan = CardsUI.cardSpan+100,
        cardsPos = {},
        cardNum = 0,
        screenWidth = UI.ScreenSize().width,
        screenHeight = UI.ScreenSize().height,
        skipBtnWH = {width=150, height=80},
        descBGHeight = CardsUI.descBGHeight,
        descBG = UI.Box.Create()
    }
    function SelectAndAddCard:CreateCardUI(cardsId)
        self.cardsUI={}
        self.SelectBG = UI.Box:Create()
        self.cardsId=cardsId
        self.nameStartY  = self.screenHeight/2-50
        self.typeStartY  = self.screenHeight/2
        self.descStartY  = self.screenHeight/2+50
        for i = 1, #self.cardsPos do
            local ui=CardsUI:CreateCardUI()
            CardsUI:SetCardUI(ui,Card:Create(CardsConfig[cardsId[i]]),self.cardsPos[i])
            table.insert(self.cardsUI,ui)
        end
        self.skipBtn = UI.Box:Create()
        self.skipBtn:Set(self.skipBtnWH)
        self.skipBtn:Set(PlayerUI.costUIBGColor[1])
        self.skipBtn:Set({x=self.screenWidth/2,y=self.screenHeight-100})

        self.skipText = DrawText:Create()
        self.skipText:Set({text="跳过",size=2,x=self.screenWidth/2,y=self.screenHeight-100})

        self.startSelect = true
        --使用怪物被选中的颜色
        self.SelectBG:Set(TargetsUI.color[3])
        self.SelectBG:Set({width = CardsUI.cardWidth+8 ,height = CardsUI.cardHeight+8})
        self.skipBtn:Adaptation("center")
        self.skipText:Adaptation("center")
        self.skip = false
        self.index = 2
        self.cardId = self.cardsId[self.index]
        self:SetSelectBG()
    end
    function SelectAndAddCard:Hide()
        self.cardsUI = nil
        self.SelectBG = nil
        self.skipBtn = nil
        self.skipText = nil
        CardsUI.HideDesc(self)
        collectgarbage()
    end
    function SelectAndAddCard:DoMove()
        local endPos = {x= ScreenCenter.x-CardsUI.cardWidth/2,y= ScreenCenter.y-CardsUI.cardHeight/2}
        CardsUI:DoMove(self.cardsUI[self.index],self.cardsPos[self.index],endPos)
        Invoke(function()
            collectgarbage()
        end,1)
    end
    function SelectAndAddCard:SetSelectBG()
        if not self.skip then
            local pos = self.cardsPos[self.index]
            local set={}
            set.x= pos.x-4
            set.y= pos.y-4
            set.width= CardsUI.cardWidth+8
            set.height= CardsUI.cardHeight+8
            self.SelectBG:Set(set)
            self.cardId = self.cardsId[self.index]
            CardsUI.ShowDesc(self,Card:Create(CardsConfig[self.cardId]))
            print("CardsUI.ShowDesc")
        else
            local data = self.skipBtn:Get()
            local set={}
            set.x= data.x-4
            set.y= data.y-4
            set.width= self.skipBtnWH.width+8
            set.height= self.skipBtnWH.height+8
            self.SelectBG:Set(set)
            CardsUI.HideDesc(self)
        end
    end
    Framework.KeyDown.A:Register(function()
        if   not SelectAndAddCard.startSelect  then return end
        if SelectAndAddCard.skip  then  return end
        SelectAndAddCard.index =  math.max(SelectAndAddCard.index - 1,1)
        SelectAndAddCard:SetSelectBG()
    end)
    Framework.KeyDown.D:Register(function()
        if   not SelectAndAddCard.startSelect  then return end
        if SelectAndAddCard.skip  then  return end
        SelectAndAddCard.index =  math.min(SelectAndAddCard.index + 1,3)
        SelectAndAddCard:SetSelectBG()
    end)
    Framework.KeyDown.W:Register(function()
        if   not SelectAndAddCard.startSelect  then return end
        SelectAndAddCard.skip = false
        SelectAndAddCard:SetSelectBG()
    end)
    Framework.KeyDown.S:Register(function()
        if   not SelectAndAddCard.startSelect  then return end
        SelectAndAddCard.skip = true
        SelectAndAddCard:SetSelectBG()
    end)
    Framework.KeyDown.MOUSE1:Register(function()
        if   not SelectAndAddCard.startSelect  then return end
        if SelectAndAddCard.skip == false then
            SelectAndAddCard:DoMove()
            UI.SignalPlus("ReturnSelectAndAddCard"..SelectAndAddCard.cardId)
        else
            UI.SignalPlus("ReturnSelectAndAddCard")
        end
        SelectAndAddCard:Hide()
        SelectAndAddCard.startSelect = false
    end)
end
--商店
CardGameShop={
    CardsId = {},
    CardsPrice = {
        [1] = {50,60}, --普通
        [2] = {70,80},  --罕见
        [3] = {90,100},  --稀有
    },
    --每次移除卡牌+25
    RemoveCardPrice = 75,
    eventId = 5,
}
for i = 4, 46 do
     table.insert(CardGameShop.CardsId,i)
end

if Game then
    function CardGameShop:GetRandomCardPrice(cardId)
        local card = Card:Create(CardsConfig[cardId])
        local priceRange=CardGameShop.CardsPrice[card.level]
        local price=math.random(priceRange[1],priceRange[2])
        if card.isLevelUp then
            price = price + 30
        end
        return price
    end
    function CardGameShop:GetCardPrice(cardId)
        return self.cardsPrice[cardId]
    end
    function CardGameShop:GetRandomCardsPrice(cardsId)
        self.cardsPrice={}
        for i = 1, #cardsId do
            self.cardsPrice[i] = CardGameShop:GetRandomCardPrice(cardsId[i])
        end
        return self.cardsPrice
    end

    function CardGameShop:GetRandomCardsId()
        self.cardsId =  SelectRandomElementInList(self.CardsId,5)
        RandomLevelUpCard(self.cardsId)
        return self.cardsId
    end
    function CardGameShop:GetBuyCard(buyCardId)
        return Card:CreateById(self.CardsId[buyCardId])
    end
    Framework.GamePlug.OnPlayerSignalPlus:Register(function(player,msg)
        local outArgs={}
        local cardGamePlayer = Players[player.index]
        if msg=="GetShopCardsInfo" then
            local  cardsId = CardGameShop:GetRandomCardsId()
            local cardsPrice = CardGameShop:GetRandomCardsPrice(cardsId)
            player:SignalPlus("ReturnShopCardsInfo"..table.concat(cardsId,",")..","..table.concat(cardsPrice,","))
            player:SignalPlus("RemoveCardPrice"..CardGameShop.RemoveCardPrice)
        end
        if Framework.GetSignalPlusArgs(msg,"BuyShopCard",outArgs) then
            local shopCardId =outArgs.numArgs[1]
            local price=CardGameShop:GetCardPrice(shopCardId)
            if player.coin<price then
                print("钱不够"..tostring(price))
                return
            end
            player.coin = player.coin - price
            cardGamePlayer.CardKnapsack:AddCard(CardGameShop:GetBuyCard(shopCardId))
            player:SignalPlus("ReturnBuyShopCard".. shopCardId)
        end
    end)
end
--Common
Common.SetNeedMoney(true)
if UI then
    Framework.UIPlug.OnSignalPlus:Register(function(msg)
        local outArgs={}
        if Framework.GetSignalPlusArgs(msg,"ReturnShopCardsInfo",outArgs) then
            local args=outArgs.numArgs
            local cardsId = {}
            local cardsPrice = {}
            for i = 1, #args/2 do
                cardsId[i] = args[i]
                cardsPrice[i] = args[#args/2+i]
            end
            CardGameShop:Create(cardsId,cardsPrice,false)
        end
        if Framework.GetSignalPlusArgs(msg,"RemoveCardPrice",outArgs) then
            local removeCardPrice=outArgs.numArgs[1]
            CardGameShop.RemoveCardPrice = removeCardPrice
        end
        if Framework.GetSignalPlusArgs(msg,"ReturnBuyShopCard",outArgs) then
            local shopCardId=outArgs.numArgs[1]
            CardGameShop:RemoveShopCard(shopCardId)
        end
    end)
    function CardGameShop:RemoveShopCard(shopCardId)
        self.cards[shopCardId] = nil
        self.cardsUI[shopCardId] = nil
        self.cardsPriceUI[shopCardId] = nil

        self:SetSelectBG()
    end
    function CardGameShop:Hide()
        self.BG = nil
        self.SelectBG = nil
        self.cardsUI = nil
        self.cardsPriceUI = nil
        self.RemoveCardText = nil
        self.RemoveCardBtn = nil
        self.RemoveCardPriceText = nil
        self.ExitBtn = nil
        self.ExitText = nil
        self.startSelect = false
        CardsUI.HideDesc(self)
        self.IsVisible = false
        collectgarbage()
        UI.StopPlayerControl(false)
    end
    function CardGameShop:Show()
        CardGameShop:Create(self.cardsId,self.cardsPrice,self.hasRemoveCard)
    end
    function CardGameShop:Create(cardsId,cardsPrice,hasRemoveCard)
        self.IsVisible = true
        self.cardsId =  cardsId
        self.cardsPrice = cardsPrice
        self.headHeight = 130
        self.tailHeight = 10
        self.spanX = 10
        self.spanY = 60
        self.index = 1
        self.BG = UI.Box:Create()
        self.SelectBG = UI.Box:Create()
        self.BG:Set(ScreenCenter)
        --5张卡宽+6个10像素间隔
        --2张卡高+4个10像素间隔
        self.BG:Set({width=CardsUI.cardWidth*5+self.spanX*6,height=self.headHeight+self.tailHeight+CardsUI.cardHeight*2+self.spanY*3,r=105,g=105,b=105,a=200})
        self.BG:Adaptation("center")
        self.nameStartY  = self.BG:Get().y+30
        self.typeStartY  = self.BG:Get().y+70
        self.descStartY  = self.BG:Get().y+100
        self.startSelect = true
        --是否移除过卡牌
        self.hasRemoveCard = hasRemoveCard
        CardKnapsackUI.OnSelectCard:Register(self.OnCardKnapsackSelect,REGISTER_MODE.once)
        self:SetPos()
        self:CreateCardUI(cardsId)
        self:SetSelectBG()
        UI.StopPlayerControl(true)
    end
    function CardGameShop.OnCardKnapsackSelect()
        print("CardGameShop.hasRemoveCard")
        CardGameShop.hasRemoveCard = true
        --CardGameShop:Hide()
        --CardGameShop:Show()
    end
    function CardGameShop:SetSelectBG()
        local ui = self.SelectBG
        local pos = self.cardPos[self.index]
        local card = self.cards[self.index]
        local set={}
        set.x= pos.x-4
        set.y= pos.y-4
        set.width=CardsUI.cardWidth+8
        set.height=CardsUI.cardHeight+8
        --使用怪物被选中的颜色
        local color = TargetsUI.color[3]
        set.r = color.r
        set.g = color.g
        set.b = color.b
        ui:Set(set)
        if card then
            CardsUI.ShowDesc(self,card)
        elseif self.index==10 and not self.hasRemoveCard then
            CardsUI.HideDesc(self)
        elseif self.index==9 then
            CardsUI.HideDesc(self)
        else
            CardsUI.HideDesc(self)
        end
    end
    function CardGameShop:SetPos()
        local startY = self.BG:Get().y+self.headHeight+self.spanY
        local startX = self.BG:Get().x+self.spanX
        self.cardPos={}
        for i = 1, 5 do
            self.cardPos[i]={x=startX+(i-1)*(CardsUI.cardWidth+self.spanX),y=startY}
            self.cardPos[5+i]={x=startX+(i-1)*(CardsUI.cardWidth+self.spanX),y=startY+CardsUI.cardHeight+self.spanY}
        end
    end
    function CardGameShop:CreateCardUI(cardsId)
        self.cardsUI={}
        self.cards = {}
        self.cardsPriceUI = {}
        for i = 1, 10 do
            if cardsId[i] then
                self.cardsUI[i] = CardsUI:CreateCardUI()
                self.cards[i] = Card:Create(CardsConfig[cardsId[i]])
                CardsUI:SetCardUI(self.cardsUI[i],self.cards[i] ,self.cardPos[i])
                self.cardsPriceUI[i] = UI.Text.Create()
                self.cardsPriceUI[i]:Set({text=tostring(self.cardsPrice[i]).."$",r=255,g=255,b=255,align="center",font="medium", x=self.cardPos[i].x,y=self.cardPos[i].y+CardsUI.cardHeight+20,width=CardsUI.cardWidth,height=30})
            end
        end
        self.RemoveCardBtn = UI.Box.Create()
        self.RemoveCardPriceText = UI.Text.Create()
        self.RemoveCardBtn:Set({r=200,g=200,b=200,width=CardsUI.cardWidth,height=CardsUI.cardHeight})
        self.RemoveCardBtn:Set(self.cardPos[10])
        self.RemoveCardText = DrawText:Create()
        if self.hasRemoveCard then
            self.RemoveCardText:Set({text="已\n售\n空",size=4})
        else
            self.RemoveCardText:Set({text="移\n除\n卡\n牌",size=3})
            self.RemoveCardPriceText:Set({text=tostring(self.RemoveCardPrice).."$",r=255,g=255,b=255,align="center",font="medium", x=self.cardPos[10].x,y=self.cardPos[10].y+CardsUI.cardHeight+20,width=CardsUI.cardWidth,height=30})
        end
        self.RemoveCardText:Set({x=self.cardPos[10].x+10,y=self.cardPos[10].y+5})

        self.ExitBtn = UI.Box.Create()
        self.ExitText = UI.Text.Create()
        self.ExitBtn:Set({r=200,g=200,b=200,width=CardsUI.cardWidth,height=CardsUI.cardHeight})
        self.ExitBtn:Set(self.cardPos[9])
        self.ExitText = DrawText:Create()
        self.ExitText:Set({text="离\n开\n商\n店",size=3})
        self.ExitText:Set({x=self.cardPos[9].x+10,y=self.cardPos[9].y+5})
    end
   function CardGameShop:GetCurCard()
       return self.cards[CardGameShop.index]
   end
    Framework.KeyDown.A:Register(function()
        if   not CardGameShop.startSelect  then return end
        CardGameShop.index =  math.max(CardGameShop.index - 1,1)
        CardGameShop:SetSelectBG()
    end)
    Framework.KeyDown.D:Register(function()
        if   not CardGameShop.startSelect  then return end
        CardGameShop.index =  math.min(CardGameShop.index + 1,10)
        CardGameShop:SetSelectBG()
    end)
    Framework.KeyDown.W:Register(function()
        if   not CardGameShop.startSelect  then return end
        if CardGameShop.index>5 then
            CardGameShop.index = CardGameShop.index - 5
        end
        CardGameShop:SetSelectBG()
    end)
    Framework.KeyDown.S:Register(function()
        if   not CardGameShop.startSelect  then return end
        if CardGameShop.index<=5 then
            CardGameShop.index = CardGameShop.index + 5
        end
        CardGameShop:SetSelectBG()
    end)
    Framework.KeyDown.MOUSE1:Register(function()
        if   not CardGameShop.startSelect  then return end
        --离开
        if CardGameShop.index==9 then
            CardGameShop:Hide()
            CardKnapsackUI.sender = nil
            UI.SignalPlus("EventEnd"..CardGameShop.eventId)
            --取消上一次的背包
            return
        end
        if CardGameShop.index==10 then
            if CardGameShop.hasRemoveCard then return end
            --删牌
            CardGameShop:Hide()
            CardKnapsackUI:Show(CardKnapsackUI.modeEnum.RemoveCard,CardGameShop)
            CardGameShop.startSelect = false
            return
        end
        --购买牌
        if CardGameShop:GetCurCard() then
            UI.SignalPlus("BuyShopCard"..CardGameShop.index)
            return
        end
    end)
end

--测试用
if UI then
    Framework.UIPlug.OnChat:Register(function(msg)
        local k=string.match(msg,"addbuff(%d+)")
        if k then
            UI.SignalPlus("addbuff"..k)
        end
        local k1=string.match(msg,"removebuffLayer")
        if k1 then
            UI.SignalPlus("removebuffLayer")
            if Player then
                Player:RemoveBuffLayer()
            end
        end
    end)
end
if Game then
    Framework.GamePlug.OnPlayerSignalPlus:Register(function(player,msg)
        local outArgs={}
        if Framework.GetSignalPlusArgs(msg,"addbuff",outArgs) then
            local buffid = outArgs.numArgs[1]
            if CurCardGame.State~=GameStateEnum.Default then
                local cardGamePlayer=Players[player.index]
                cardGamePlayer:AddBuff(CardGameBuff:Create(buffid,1))
            end
        end
        if Framework.GetSignalPlusArgs(msg,"removebuffLayer",outArgs) then
            if CurCardGame.State~=GameStateEnum.Default then
                local cardGamePlayer=Players[player.index]
                cardGamePlayer:RemoveBuffLayer()
            end
        end
    end)
end
