print("=========Model load========")
GameStateEnum={
    Default = 0,
    --发牌阶段
    RoundStartState = 1,
    --玩家攻击阶段
    PlayerAttackState = 2,
    --玩家回合结束阶段
    PlayerRoundEndState = 3,
    --怪物攻击阶段
    MonsterAttackState = 4,
    --怪物回合结束阶段
    MonsterRoundEndState = 5,
    --回合终止
    RoundOverState = 6,
}

ControlEnum ={
    Left = 0,
    Right = 1,
}
CardType={
    [1]="攻击",
    [2]="技能",
    [4]="能力",
    [16]="消耗",
}

CardTypeEnum={
    Attack = 1,
    Skill = 2,
    Ability = 4,
    Deplete = 16,
}

--单攻0/群攻1/自身2
CardTargetEnum={
    Single = 0,
    Multiple = 1,
    Self = 2
}

BuffEnum={
    Default = 0,
    --重伤:受伤时伤害增加50%
    Hurt = 1,
    --虚弱:攻击时伤害减少30%
    Week = 1<<1,

    --变为单体（只能给玩家施加）
    ToSingle = 1<<2,
    --变为群体（只能给玩家施加）
    ToMultiple = 1<<3,
    --中毒
    Poison = 1<<4,
    --涂毒
    AttackPoison = 1<<5,
    --涂毒+
    AttackPoisonPlus = 1<<6,
    --防御姿态
    DefensiveStance = 1<<7,
    --自残
    SelfHarm = 1<<8,
    --转移
    Transfer = 1<<9,
    --复苏
    Recovery = 1<<10,
    --伤害守恒
    DamageConservation = 1<<11,
    --回收
    Reclaim = 1<<12,
    --贷款
    Loan = 1<<13,
    --反甲
    ThornMail = 1<<14,
    --伤口
    Wounds = 1<<15,
    --分段射击
    SegmentShoot = 1<<16,
    --所有Debuff:重伤,虚弱,中毒
    AllDeBuff = 1|1<<1|1<<4,
}
BuffEnumMap={
    BuffEnum.Hurt,
    BuffEnum.Week ,
    BuffEnum.ToSingle,
    BuffEnum.ToMultiple ,
    BuffEnum.Poison ,
    BuffEnum.AttackPoison ,
    BuffEnum.AttackPoisonPlus ,
    BuffEnum.DefensiveStance ,
    BuffEnum.SelfHarm ,
    BuffEnum.Transfer ,
    BuffEnum.Recovery ,
    BuffEnum.DamageConservation ,
    BuffEnum.Reclaim ,
    BuffEnum.Loan ,
    BuffEnum.ThornMail ,
    BuffEnum.Wounds ,
    BuffEnum.SegmentShoot ,
    BuffEnum.AllDeBuff,
}
CardGameEvent={
    [1]={1,"接下来三回合怪物只有1血"},
    [2]={2,"移除一张牌"},
    [3]={3,"升级一张牌"},
    [4]={4,"事件三选一"},
    [5]={5,"商店"},
}
CardGameTreasureEnum={
    [1]="拉尼的祝福" -- "接下来三回合怪物只有1血"
}
CardsConfig ={
    --名称 = id,花费,攻击,护甲,buffId,buffLayer,CardType,单攻0/群攻1/自身2,稀有程度(1,2,3)
    [1]={1,1,6,0,0,0,1,0,1}, --射击
    [2]={2,1,0,5,0,0,2,CardTargetEnum.Self,1},--防御
    [3]={3,2,8,0,1,2,1,0,1},--恶意中伤
    [4]={4,1,8,0,BuffEnum.Week,1,CardTypeEnum.Attack,0,1},--致盲射击
    [5]={5,1,8,0,0,0,1,1,1},--霰弹枪
    [6]={6,0,6,0,0,0,16,1,2},--手榴弹
    [7]={7,1,0,0,0,0,CardTypeEnum.Skill,CardTargetEnum.Self,2},--换弹
    [8]={8,0,0,0,BuffEnum.Week,2,CardTypeEnum.Deplete,CardTargetEnum.Multiple,2},--闪光弹
    [9]={9,1,-5,0,0,0,CardTypeEnum.Deplete,CardTargetEnum.Self,2},--急救包
    [10]={10,1,0,0,BuffEnum.ToSingle,1,CardTypeEnum.Deplete,CardTargetEnum.Self,2},--收束器
    [11]={11,1,0,0,BuffEnum.ToMultiple,1,CardTypeEnum.Deplete,CardTargetEnum.Self,2},--扩散器
    [12]={12,2,16,0,0,0,CardTypeEnum.Attack,CardTargetEnum.Single,2},--狙击枪
    [13]={13,0,0,0,BuffEnum.Poison,3,CardTypeEnum.Deplete,CardTargetEnum.Multiple,2},--毒气弹
    [14]={14,-1,0,0,0,0,CardTypeEnum.Deplete,CardTargetEnum.Self,2},--能量饮料
    [15]={15,1,0,0,BuffEnum.AttackPoison,1,CardTypeEnum.Skill,CardTargetEnum.Self,2},--涂毒
    [16]={16,2,0,0,BuffEnum.DefensiveStance,1,CardTypeEnum.Skill,CardTargetEnum.Self,1},--防御姿态
    [17]={17,2,0,0,0,0,CardTypeEnum.Skill,CardTargetEnum.Single,1},--针对防御
    -- |轻机枪 |18/118|1   |1x8/1x11|   |      |      |攻击   |  单体 |1      |        |
    [18]={18,1,1,0,0,0,CardTypeEnum.Attack,CardTargetEnum.Single,1,8},
    [19]={19,1,0,12,0,0,2,CardTargetEnum.Self,1},--重甲
    [20]={20,0,0,0,BuffEnum.Hurt,2,CardTypeEnum.Deplete,CardTargetEnum.Multiple,2},--燃烧弹
    -- |折磨|21/121  |0    |0    |      |       |     |技能  |  单体 |1      |目标所有效果层数翻倍|
    [21]={21,1,0,0,0,0,CardTypeEnum.Skill,CardTargetEnum.Single,1},--折磨
    --  |毒性爆发|22/122|1/0  |0    |     |        |     |技能  |  单体 |1      |立刻造成目标中毒层数伤害|
    [22]={22,1,0,0,0,0,CardTypeEnum.Skill,CardTargetEnum.Single,1},--毒性爆发
    --|自残  |23/123 |-2/-3|     |     |自残   |1     |技能  |自身   |2     |所有卡牌目标变为自身，抽(3/5)张牌|
    [23]={23,-2,0,0,BuffEnum.SelfHarm,1,CardTypeEnum.Skill,CardTargetEnum.Self,2},--自残
    --|伤害转移|24/124|3/2  |     |    |伤害转移|1   |消耗  |单体     |3     |由效果目标承受伤害，有多个目标时分散伤害|
    [24]={ 24, 3, 0, 0, BuffEnum.Transfer, 1, CardTypeEnum.Deplete, CardTargetEnum.Single, 2},--伤害转移
    --|盾击|25/125|1/0     |     |    |        |   |攻击  |单体     |1      |造成护盾值伤害|
    [25]={25,1,0,0,0,0,CardTypeEnum.Attack,CardTargetEnum.Single,1},
    --|复苏|26/126|1/0     |     |    | 复苏   |3   |技能 |自身      |2     |每回合恢复复苏层数的血量|
    [26]={26,1,0,0,BuffEnum.Recovery,3,CardTypeEnum.Skill,CardTargetEnum.Self,2},--复苏
    --|双重释放|27/127|1    |    |     |       |    |技能 | 自身     |2     |本回合下（1/2）张攻击牌攻击段数x2|
    [27]={27,1,0,0,0,0,CardTypeEnum.Skill,CardTargetEnum.Self,2},--双重释放
    --|巩固|28/128|2/1      |    |    |        |     |技能 | 自身    |2      |护盾翻倍|
    [28]={28,2,0,0,0,0,CardTypeEnum.Skill,CardTargetEnum.Self,2},--巩固
    --|伤害守恒|29/129|2/1  |    |    |伤害守恒  |1   |技能| 自身    |2      |目标受到的伤害转换为自身血量|
    [29]={29,2,0,0,BuffEnum.DamageConservation,1,CardTypeEnum.Skill,CardTargetEnum.Self,2},
    --|恶魔交易|30/130|-3/-5|5/11 |    |          |   |攻击| 自身     |2      ||
    [30]={30,-3,5,0,0,0,CardTypeEnum.Attack,CardTargetEnum.Self,2},
    --|天使交易|31/131|1   |-5/-11|    |          |   |攻击| 自身     |2      ||
    [31]={31,1,-5,0,0,0,CardTypeEnum.Attack,CardTargetEnum.Self,2},
    --|回收|32/132    |2/1 |0     |    |回收      |1  |技能| 自身     |2      |消耗类型卡牌不会消耗|
    [32]={32,2,0,0,BuffEnum.Reclaim,1,CardTypeEnum.Skill,CardTargetEnum.Self,2},
    --|贷款|33/133    |2/1 |     |     |贷款      |1  |消耗|自身      |3      |本回合内可以预支付能量，最多预支6倍最大能量|
    [33]={33,2,0,0,BuffEnum.Loan,1,CardTypeEnum.Deplete,CardTargetEnum.Self,3},
    --|专注|34/134    |1/0 |     |     |         |    |消耗|自身      |1      |最大能量加1|
    [34]={34,1,0,0,0,0,CardTypeEnum.Deplete,CardTargetEnum.Self,1},
    --|孤注一掷|35/135 |1  | 3   |     |         |    |消耗|单体     |3      |每次拿到手中时伤害增加（6/8）|
    [35]={35,1,3,0,0,0,CardTypeEnum.Deplete,CardTargetEnum.Single,3},
    --|起源弹  |36/136 |1  |  3  |     |         |    |消耗|群体     |3      |对目标施加所有减益效果（1/3）层|
    [36]={36,1,3,0,BuffEnum.AllDeBuff,1,CardTypeEnum.Deplete,CardTargetEnum.Self,3},
    --|反甲|37/137    |1   | 3/5 |      | 反甲    | 3/5|技能|自身     |1      |对攻击者造成效果层数伤害|
    [37]={37,1,0,3,BuffEnum.ThornMail,3,CardTypeEnum.Skill,CardTargetEnum.Self,1},
    --|撕裂|38/138    |1   | 3/5  |      | 伤口    | 3/5|攻击| 单体    | 1     |目标被攻击时造成效果层数的伤害|
    [38]={38,1,3,0,BuffEnum.Wounds,3,CardTypeEnum.Attack,CardTargetEnum.Single,1},
    --|印刷机|39/139  |1   |    |      |         |    |消耗|自身     | 2     |复制(1/3)张下一次打出的牌|
    [39]={39,1,0,0,0,0,CardTypeEnum.Deplete,CardTargetEnum.Self,2},
    --|净化| 40/140   |1/0|     |      |         |    |消耗|自身     | 2     |移除自身所有的负面效果|
    [40]={40,1,0,0,0,0,CardTypeEnum.Deplete,CardTargetEnum.Self,2},
    --|偷窃| 41/141   |1   |3 |      |         |    |攻击|单体     | 2     |偷窃（10~20/20~40）金币   |
    [41]={41,1,3,0,0,0,CardTypeEnum.Attack,CardTargetEnum.Single,2},
    --|金钱之力|42/142 |3/2  |    |     |          |    |消耗|单体     | 3    |造成金币数量伤害|
    [42]={42,3,0,0,0,0,CardTypeEnum.Deplete,CardTargetEnum.Single,2},
    --|追击|43/143    |1    |8/10|     |          |    |攻击|单体     | 1     |抽一张牌，如果目标有伤口再抽一张|
    [43]={43,1,8,0,0,0,CardTypeEnum.Attack,CardTargetEnum.Single,1},
    --|调整|44/144    |1    |    |8/10  |        |     |技能|自身     |1      |选择一张牌放到抽牌堆顶部|
    [44]={44,1,0,8,0,0,CardTypeEnum.Skill,CardTargetEnum.Self,1},
    --|粉碎|45/144   |1     |    |8/10  |        |     |技能|自身     | 1    | 选择一张牌消耗        |
    [45]={45,1,0,10,0,0,CardTypeEnum.Skill,CardTargetEnum.Self,1},
    --|分段射击|46/146|1/0   |   |      | 分段射击|  1  |技能|自身     |1     | 攻击卡牌伤害/2，段数X2 |
    [46]={46,1,0,0,BuffEnum.SegmentShoot,1,CardTypeEnum.Skill,CardTargetEnum.Self,1},

    --升级后的配置
    [101]={101,1,9,0,0,0,1,0,1},--射击+
    [102]={102,1,0,8,0,0,2,CardTargetEnum.Self,1},--防御+
    [103]={103,2,10,0,1,3,1,0,1},--恶意中伤+
    [104]={104,1,11,0,BuffEnum.Week,1,CardTypeEnum.Attack,0,1},--致盲射击+
    [105]={105,1,11,0,0,0,1,1,1},--霰弹枪+
    [106]={106,0,9,0,0,0,16,1,2},--手榴弹+
    [107]={107,0,0,0,0,0,CardTypeEnum.Skill,CardTargetEnum.Self,2},--换弹
    [108]={108,0,0,0,BuffEnum.Week,4,CardTypeEnum.Deplete,1,2},--闪光弹+
    [109]={109,1,-11,0,0,0,CardTypeEnum.Deplete,CardTargetEnum.Self,2},--急救包+
    [110]={110,0,0,0,BuffEnum.ToSingle,1,CardTypeEnum.Deplete,CardTargetEnum.Self,2},--收束器
    [111]={111,0,0,0,BuffEnum.ToMultiple,1,CardTypeEnum.Deplete,CardTargetEnum.Self,2},--扩散器
    [112]={112,2,22,0,0,0,CardTypeEnum.Attack,CardTargetEnum.Single,2},--狙击枪
    [113]={113,0,0,0,BuffEnum.Poison,4,CardTypeEnum.Deplete,CardTargetEnum.Multiple,2},--毒气弹
    [114]={114,-2,0,0,0,0,CardTypeEnum.Deplete,CardTargetEnum.Self,2},--能量饮料
    [115]={115,1,0,0,BuffEnum.AttackPoisonPlus,1,CardTypeEnum.Skill,CardTargetEnum.Self,2},--涂毒
    [116]={116,2,0,0,BuffEnum.DefensiveStance,2,CardTypeEnum.Skill,CardTargetEnum.Self,1},--防御姿态
    [117]={117,1,0,0,0,0,CardTypeEnum.Skill,CardTargetEnum.Single,1},--针对防御
    [118]={118,1,1,0,0,0,CardTypeEnum.Attack,CardTargetEnum.Single,1,11}, --射击
    [119]={119,1,0,16,0,0,2,CardTargetEnum.Self,1},--重甲
    [120]={120,0,0,0,BuffEnum.Hurt,4,CardTypeEnum.Deplete,CardTargetEnum.Multiple,2},--燃烧弹
    [121]={121,1,0,0,0,0,CardTypeEnum.Skill,CardTargetEnum.Single,2},--折磨
    [122]={122,0,0,0,0,0,CardTypeEnum.Skill,CardTargetEnum.Single,1},--毒性爆发
    [123]={123,-3,0,0,BuffEnum.SelfHarm,1,CardTypeEnum.Skill,CardTargetEnum.Self,2},--自残
    --|伤害转移|24/124|3/2  |     |    |伤害转移|1   |消耗  |单体     |3     |由效果目标承受伤害，有多个目标时分散伤害|
    [124]={ 124, 2, 0, 0, BuffEnum.Transfer, 1, CardTypeEnum.Deplete, CardTargetEnum.Single, 2},--伤害转移
    [125]={125,0,0,0,0,0,CardTypeEnum.Attack,CardTargetEnum.Single,1},
    --|复苏|26/126|1/0     |     |    | 复苏   |3   |技能 |自身      |2     |每回合恢复复苏层数的血量|
    [126]={126,0,0,0,BuffEnum.Recovery,3,CardTypeEnum.Skill,CardTargetEnum.Self,2},--复苏
    --|双重释放|27/127|1    |    |     |       |    |技能 | 自身     |2     |本回合下（1/2）张攻击牌攻击段数x2|
    [127]={127,1,0,0,0,0,CardTypeEnum.Skill,CardTargetEnum.Self,2},--双重释放
    --
    [128]={128,1,0,0,0,0,CardTypeEnum.Skill,CardTargetEnum.Self,2},--巩固
    --
    [129]={129,1,0,0,BuffEnum.DamageConservation,1,CardTypeEnum.Skill,CardTargetEnum.Self,2},
    [130]={130,-5,11,0,0,0,CardTypeEnum.Attack,CardTargetEnum.Self,2},
    [131]={131,1,-11,0,0,0,CardTypeEnum.Attack,CardTargetEnum.Self,2},
    [132]={132,1,0,0,BuffEnum.Reclaim,2,CardTypeEnum.Skill,CardTargetEnum.Self,2},
    [133]={133,1,0,0,BuffEnum.Loan,1,CardTypeEnum.Deplete,CardTargetEnum.Self,3},
    [134]={134,0,0,0,0,0,CardTypeEnum.Deplete,CardTargetEnum.Self,1},
    [135]={135,1,3,0,0,0,CardTypeEnum.Deplete,CardTargetEnum.Single,3},
    [136]={136,1,3,0,BuffEnum.AllDeBuff,3,CardTypeEnum.Deplete,CardTargetEnum.Multiple,3},
    [137]={137,1,0,5,BuffEnum.ThornMail,5,CardTypeEnum.Skill,CardTargetEnum.Self,1},
    [138]={138,1,5,0,BuffEnum.Wounds,5,CardTypeEnum.Attack,CardTargetEnum.Single,1},
    [139]={139,1,0,0,0,0,CardTypeEnum.Deplete,CardTargetEnum.Self,2},
    [140]={140,0,0,0,0,0,CardTypeEnum.Deplete,CardTargetEnum.Self,2},
    [141]={141,1,3,0,0,0,CardTypeEnum.Attack,CardTargetEnum.Single,2},
    [142]={142,2,0,0,0,0,CardTypeEnum.Deplete,CardTargetEnum.Single,2},
    [143]={143,1,10,0,0,0,CardTypeEnum.Attack,CardTargetEnum.Single,1},
    [144]={144,1,0,10,0,0,CardTypeEnum.Skill,CardTargetEnum.Self,1},
    [145]={145,1,0,10,0,0,CardTypeEnum.Skill,CardTargetEnum.Self,1},
    [146]={146,0,0,0,BuffEnum.SegmentShoot,1,CardTypeEnum.Skill,CardTargetEnum.Self,1},

}

CardsDesc={
    [1]={"射击","造成 %d 伤害"},
    [2]={"防御","防御 %d 伤害"},
    [3]={"恶意中伤","造成 %d 伤害\n施加 2 层重伤"},
    [4]={"致盲射击","造成 %d 伤害\n施加 1 层虚弱"},
    [5]={"霰弹枪","对所有目标造成 %d 伤害"},
    [6]={"手榴弹","对所有目标造成 %d 伤害"},
    [7]={"换弹","抽取 2 张牌"},
    [8]={"闪光弹","对所有目标施加 2 层虚弱"},
    [9]={"急救包","对自身造成 %d 点伤害"},
    [10]={"收束器","群体卡牌变为单体\n根据场上目标集中伤害"},
    [11]={"扩散器","单体卡牌变为群体\n根据场上目标分散伤害"},
    [12]={"狙击枪","对目标造成 %d 伤害"},
    [13]={"毒气弹","对所有目标造成 3 层中毒"},
    [14]={"能量饮料","恢复 1 点能量"},
    [15]={"涂毒","攻击卡牌对目标施加 1 层中毒"},
    [16]={"防御姿态","1 回合内生成卡牌伤害护甲"},
    [17]={"针对防御","根据目标攻击力生成护甲"},
    [18]={"轻机枪","造成 %d 伤害 %d 次"},
    [19]={"重甲","防御 %d 伤害"},
    [20]={"燃烧弹","对所有目标施加 2 层重伤"},
    [21]={"折磨","目标所有效果层数 X2"},
    [22]={"毒性爆发","立刻造成目标中毒层数伤害"},
    [23]={"自残","所有卡牌目标变为自身\n抽 3 张牌"},
    [24]={"转移","由效果目标承受伤害和效果\n有多个目标时分散伤害和效果"},
    [25]={"盾击","造成护盾值伤害"},
    [26]={"复苏","每回合恢复复苏层数的血量"},
    [27]={"双重释放","本回合下 1 张攻击牌攻击段数X2"},
    [28]={"巩固","护盾翻倍"},
    [29]={"伤害守恒","目标受到的伤害转换为自身血量"},
    [30]={"恶魔交易","对自身造成 %d 点伤害\n恢复 3 点能量"},
    [31]={"天使交易","对自身造成 %d 点伤害"},
    [32]={"回收","1 回合内消耗类型卡牌不会消耗"},
    [33]={"贷款","1 回合内可以预支付能量\n最多预支4倍最大能量"},
    [34]={"专注","最大能量加 1"},
    [35]={"孤注一掷","每次拿到手中时伤害增加 6"},
    [36]={"起源弹","对所有目标施加所有减益效果 1 层"},
    [37]={"反甲","防御 %d 伤害\n对攻击者造成效果层数伤害"},
    [38]={"撕裂","造成 %d 伤害施加 3 层伤口\n目标被攻击时造成效果层数的伤害"},
    [39]={"印刷机","复制 1 张下一次打出的牌"},
    [40]={"净化","移除自身所有的负面效果"},
    [41]={"偷窃","偷窃 10~20 金币"},
    [42]={"金钱之力","造成金币数量伤害"},
    [43]={"追击","抽一张牌\n如果目标有伤口再抽一张"},
    [44]={"调整","防御 %d 伤害\n选择一张牌放到抽牌堆顶部"},
    [45]={"粉碎","防御 %d 伤害\n选择一张牌消耗"},
    [46]={"分段射击","1 回合内攻击卡牌伤害/2 段数X2"},
}
CardsUpdateDesc={
    [101]={"射击+",CardsDesc[1][2]},
    [102]={"防御+",CardsDesc[2][2]},
    [103]={"恶意中伤+","造成 %d 伤害\n施加 3 层重伤"},
    [104]={"致盲射击+","对所有目标造成 %d 伤害\n施加 1 层虚弱"},
    [105]={"霰弹枪+",CardsDesc[5][2]},
    [106]={"手榴弹+",CardsDesc[6][2]},
    [107]={"换弹+",CardsDesc[7][2]},
    [108]={"闪光弹+","对所有目标施加 4 层虚弱"},
    [109]={"急救包+",CardsDesc[9][2]},
    [110]={"收束器+",CardsDesc[10][2]},
    [111]={"扩散器+",CardsDesc[11][2]},
    [112]={"狙击枪+",CardsDesc[12][2]},
    [113]={"毒气弹+","对所有目标造成 4 层中毒"},
    [114]={"能量饮料+","恢复 2 点能量"},
    [115]={"涂毒+","攻击卡牌对目标施加 3 层中毒"},
    [116]={"防御姿态+","2 回合内生成卡牌伤害护甲"},
    [117]={"针对防御+",CardsDesc[17][2]},
    [118]={"轻机枪+","造成 %d 伤害 %d 次"},
    [119]={"重甲+","防御 %d 伤害"},
    [120]={"燃烧弹+","对所有目标施加 4 层重伤"},
    [121]={"折磨+","目标所有效果层数 X3"},
    [122]={"毒性爆发+",CardsDesc[22][2]},
    [123]={"自残+","所有卡牌目标变为自身\n抽 4 张牌"},
    [124]={"转移+",CardsDesc[24][2]},
    [125]={"盾击+",CardsDesc[25][2]},
    [126]={"复苏+",CardsDesc[26][2]},
    [127]={"双重释放+","本回合下 2 张攻击牌攻击段数X2"},
    [128]={"巩固+",CardsDesc[28][2]},
    [129]={"伤害守恒+",CardsDesc[29][2]},
    [130]={"恶魔交易+","对自身造成 %d 点伤害\n恢复 5 点能量"},
    [131]={"天使交易+",CardsDesc[31][2]},
    [132]={"回收+","2 回合内消耗类型卡牌不会消耗"},
    [133]={"贷款+",CardsDesc[33][2]},
    [134]={"专注+",CardsDesc[34][2]},
    [135]={"孤注一掷+","每次拿到手中时伤害增加 8"},
    [136]={"起源弹+","对所有目标施加所有减益效果 3 层"},
    [137]={"反甲+",CardsDesc[37][2]},
    [138]={"撕裂+","造成 %d 伤害施加 5 层伤口\n目标被攻击时造成效果层数的伤害"},
    [139]={"印刷机+","复制 3 张下一次打出的牌"},
    [140]={"净化+",CardsDesc[40][2]},
    [141]={"偷窃+","偷窃 20~40 金币"},
    [142]={"金钱之力+",CardsDesc[42][2]},
    [143]={"追击+",CardsDesc[43][2]},
    [144]={"调整+","防御 %d 伤害\n选择一张牌放到抽牌堆顶部"},
    [145]={"粉碎+","防御 %d 伤害\n选择一张牌消耗"},
    [146]={"分段射击+","1 回合内攻击卡牌伤害/2 段数X2"},
}
CardsDesc = MergeHashTables(CardsDesc,CardsUpdateDesc)



CardGameBuff={
    layer = 0,
    id = BuffEnum.Default
}
function CardGameBuff:Copy()
    local buff={}
    setmetatable(buff,self)
    self.__index = self
    buff.id = self.id
    buff.layer = self.layer
    return buff
end
function CardGameBuff:Create(id,layer)
    local buff={}
    setmetatable(buff,self)
    self.__index = self
    buff.id = id
    buff.layer = layer or 1
    return buff
end
function CardGameBuff.Week(damage)
   return math.floor(damage * 0.7)
end
function CardGameBuff.Hurt(damage)
    return damage + math.ceil(damage*0.5)
end
Card={
    id = 0,
    name = "",
    cost = 1,
    damage = 0,
    armor = 1,
    desc= "",
    cardType = 1,
    buffLayer = 0,
    OnUse = REGISTER:New(),
    OnUsed = REGISTER:New(),
    OnSelected = REGISTER:New(),
    OnSelectUseCard = REGISTER:New(),
    OnAddCard = REGISTER:New(),
}
function Card:CreateById(cardId)
    return self:Create(CardsConfig[cardId])
end
function Card:Create(cardConfig)
    local card = {}
    setmetatable(card,self)
    self.__index = self
    card.id = tonumber(cardConfig[1])
    card.cost = tonumber(cardConfig[2])
    card.damage = tonumber(cardConfig[3])
    card.armor = tonumber(cardConfig[4])
    card.desc = CardsDesc[card.id][1]
    card.name = CardsDesc[card.id][2]
    card.buff =  CardGameBuff:Create(tonumber(cardConfig[5]),tonumber(cardConfig[6]))
    card.cardType = tonumber(cardConfig[7])
    card.cardTarget = tonumber(cardConfig[8])
    card.level = tonumber(cardConfig[9])
    card.attackCount =tonumber(cardConfig[10]) or 1
    card.isLevelUp =  (card.id>100)
    return card
end
if Game then
    function CardGameBuff:AddLayer(layer)
        self.layer = self.layer + layer
    end
    function CardGameBuff:Remove()
        self.layer = 0
    end
    function CardGameBuff:RemoveLayer(layer)
        --默认减少一层
        layer = layer or 1
        self.layer = self.layer - layer
        if  self.layer < 0 then
            self.layer = 0
        end
    end
    function CardGameBuff:ToString()
        return table.concat({
            self.id,
            self.layer,
        }, ",") -- 直接连接每个怪兽的属性
    end
    OccupationEnum={
        Soldier = 0,
    }
    CardGame={
        State = GameStateEnum.Default,
        roundNum = 0,
        OnCardGameInit = REGISTER:New(),
        OnRoundStart = REGISTER:New(),
        OnPlayerAttack = REGISTER:New(),
        OnPlayerRoundEnd = REGISTER:New(),
        OnMonsterAttack = REGISTER:New(),
        OnMonsterAttacked = REGISTER:New(),
        OnMonsterRoundEnd = REGISTER:New(),
        OnRoundOver = REGISTER:New(),
        OnGameOver = REGISTER:New(),
        level = "1-1"
    }
    function CardGame:Create()
        local cardGame = {}
        self.randomSeed = Game.RandomInt(10000,90000)
        --TODO 测试先写死
        --math.randomseed(self.randomSeed)
        math.randomseed(11111)
        setmetatable(cardGame,self)
        self.__index = self
        return cardGame
    end
    function CardGame:Start(cardGamePlayer,level)
        self.level = level
        print("CardGame:Start",self.State)
        if self.State==GameStateEnum.Default or self.State == GameStateEnum.RoundOverState then
            self.OnCardGameInit:Trigger(cardGamePlayer,level)
            Invoke(function(cardGame,_cardGamePlayer)
                cardGame:RoundStart(_cardGamePlayer)
            end,0.5,{self,cardGamePlayer})
        end
    end
    function CardGame:RoundStart(cardGamePlayer)
        if self.State==GameStateEnum.RoundOverState or self.State==GameStateEnum.Default or self.State==GameStateEnum.MonsterRoundEndState then
            self.roundNum = self.roundNum + 1
            self.OnRoundStart:Trigger(cardGamePlayer,self.roundNum,self.level)
            self.State = GameStateEnum.RoundStartState
        end
    end
    function CardGame:PlayerAttack(cardGamePlayer)
        if self.State==GameStateEnum.RoundStartState then
            self.OnPlayerAttack:Trigger(cardGamePlayer)
            self.State = GameStateEnum.PlayerAttackState
        end
    end
    function CardGame:PlayerRoundEnd(cardGamePlayer)
        if self.State==GameStateEnum.PlayerAttackState then
            self.OnPlayerRoundEnd:Trigger(cardGamePlayer)
            self.State = GameStateEnum.PlayerRoundEndState
        end
    end
    function CardGame:MonsterAttack(cardGamePlayer)
        if self.State==GameStateEnum.PlayerRoundEndState then
            self.OnMonsterAttack:Trigger(cardGamePlayer)
            self.State = GameStateEnum.MonsterAttackState
            self.OnMonsterAttacked:Trigger(cardGamePlayer)
        end
    end
    function CardGame:MonsterRoundEnd(cardGamePlayer)
        if self.State==GameStateEnum.MonsterAttackState then
            self.OnMonsterRoundEnd:Trigger(cardGamePlayer)
            self.State = GameStateEnum.MonsterRoundEndState
        end
    end

    function CardGame:RoundOver(cardGamePlayer)
        if  self.State ~= GameStateEnum.RoundOverState then
            self.OnRoundOver:Trigger(cardGamePlayer,self.level)
            self.State = GameStateEnum.RoundOverState
            self.RoundNum = 0
        end
    end

    CardGameMonsters = {
        OnRoundStart = REGISTER:New(),
        OnAllDeath = REGISTER:New(),
        OnCreate = REGISTER:New(),
        OnSetAction = REGISTER:New(),
        OnTakeDamageEnd = REGISTER:New(),
        monsters = {}
    }

    function CardGameMonsters:RoundStart()
        self:RemoveBuffLayer()
        for i = 1, #self.monsters do
            --移除怪物护甲?
            self.monsters[i].armor = 0
            CardGameMonsters.OnRoundStart:Trigger(self.monsters[i])
        end
    end
    function CardGameMonsters:Clear()
        for i = 1, #self.monsters do
            self.monsters[i]:Clear()
            self.monsters[i] = nil
        end
    end
    function CardGameMonsters:GetAliveAll()
        local monsters ={}
        for i = 1, #self.monsters do
            if self.monsters[i].health>0 then
                monsters[i] = self.monsters[i]
            end
        end
        return monsters
    end
    function CardGameMonsters:GetAll()
        local monsters ={}
        for i = 1, #self.monsters do
            monsters[i] = self.monsters[i]
        end
        return monsters
    end
    function CardGameMonsters:TakeDamage(cardGamePlayer)
        local aliveCount = 0
        for i = 1, #self.monsters do
            if self.monsters[i].health>0 then
                aliveCount=aliveCount+1
                Invoke(function(monster,player)
                    monster:TakeDamage(player)
                end,1*aliveCount,{self.monsters[i],cardGamePlayer})
            end
        end
        --延迟存活的怪物(数量+1)s后告知CardGame结束回合
        Invoke(function(player)
            CardGameMonsters.OnTakeDamageEnd:Trigger(player)
        end,aliveCount+1,cardGamePlayer)
    end
    function CardGameMonsters:Add(cardGameMonster)
        table.insert(self.monsters,cardGameMonster)
        cardGameMonster.index = #self.monsters
        self.allDeath = false
    end
    function CardGameMonsters:GetByIndex(monsterIndex)
        if not self.monsters[monsterIndex] then return
            print("1001 error not index"..monsterIndex)
        end
        return self.monsters[monsterIndex]
    end
    function CardGameMonsters:StorageAction(cardGamePlayer,roundNum,level)
        for i = 1, #self.monsters do
            CardGameMonsters.OnSetAction:Trigger(cardGamePlayer,self.monsters[i],roundNum,level)
            self.monsters[i]:StorageAction(cardGamePlayer,roundNum,level)
        end
    end
    function CardGameMonsters:Count()
        return #self.monsters
    end
    function CardGameMonsters:AliveCount()
        local aliveCount = 0
        for i = 1, #self.monsters do
            if self.monsters[i].health>0 then
                aliveCount=aliveCount+1
            end
        end
        return aliveCount
    end
    function CardGameMonsters:RemoveBuffLayer()
        for i = 1, #self.monsters do
            self.monsters[i]:RemoveBuffLayer()
        end
    end
    function CardGameMonsters:CheckAllDeath(cardGamePlayer)
        local deathCount = 0
        for i = 1, #self.monsters do
            if self.monsters[i].health==0 then
                deathCount = deathCount +1
            end
        end
        if deathCount== #self.monsters then
            return true
        else
            return false
        end
    end
    CardGameEntity={
        OnAttacked = REGISTER:New() --args:attacker,victim,damage,buff
    }
    function CardGameEntity.TakeDamage(attacker,victim)
        --这里要在1s内实现多段伤害，每段伤害都能上buff
        local time = 1/(attacker.attackCount+0.0)
        local attackBuff = nil
        if attacker.attackBuff then
            attackBuff = attacker.attackBuff:Copy()
        end
        local attack=attacker.attack
        for i = 1, attacker.attackCount do
            Invoke(function()
                local attackBuffs={}
                local index=1
                if attackBuff then
                    local attackBuffId = attackBuff.id
                    local attackBuffLayer = attackBuff.layer
                    while attackBuffId and attackBuffId>0 do
                        local temp=attackBuffId%2
                        attackBuffId=math.floor(attackBuffId/2)
                        if temp==1 then
                            table.insert(attackBuffs,CardGameBuff:Create(BuffEnumMap[index],attackBuffLayer))
                        end
                        index=index+1
                    end
                end

                --计算虚弱重伤
                local damage = victim:Attacked(attacker,attack,attackBuff)
                attacker.OnTakeDamage:Trigger(victim,damage)
                if victim:IsMonster() and attacker:IsPlayer() then
                    --显示伤害
                    attacker:SignalPlus("ShowDamage".. victim.index ..","..damage)
                    --同步buff ui
                    for j = 1, #attackBuffs do
                        local buff = attackBuffs[j]
                        if buff then
                            local monsterBuff = victim:GetBuff(buff.id)
                            if monsterBuff and not victim.death then
                                attacker:SignalPlus("AddMonsterBuff"..monsterBuff:ToString()..",".. victim.index)
                            end
                        end
                    end
                end
                local cardGamePlayer
                if victim:IsPlayer() then
                    cardGamePlayer = victim
                end
                if attacker:IsPlayer()then
                    cardGamePlayer = attacker
                end
                if cardGamePlayer then
                    local cardGameMonsters = CardGameMonsters:GetAll()
                    for entityIndex, cardGameEntity in pairs(cardGameMonsters) do
                        cardGamePlayer:SignalPlus("ReturnTargetInfo".. cardGameEntity:ToString())
                    end
                end
            end,time*(i-1))
        end
        attacker.OnTakeDamageAni:Trigger()
        -- 攻击后移除攻击附带buff
        attacker.attackBuff = nil
    end

    function CardGameEntity.Hurt(victim,damage)
        if victim.death then return 0 end
        if damage>0  then
            if damage>victim.armor then
                damage = damage - victim.armor
                victim.health =  victim.health - damage
                victim.armor = 0
            else
                victim.armor = victim.armor - damage
                damage = 0
            end
        elseif damage<0 then
            victim.health =  victim.health - damage
            victim.health =  math.min(victim.maxHealth,victim.health)
        end

        if victim.health<=0 then
            victim.health = 0
        end
        return damage
    end

    function CardGameEntity.Attacked(victim,attacker,damage,buff)

        --虚弱减伤
        if attacker and CardGameEntity.GetBuff(attacker,BuffEnum.Week) then
            damage = CardGameBuff.Week(damage)
        end
        --重伤增伤50%
        if victim and CardGameEntity.GetBuff(victim,BuffEnum.Hurt) then
            damage = CardGameBuff.Hurt(damage)
        end
        damage = CardGameEntity.Hurt(victim,damage)

        if victim.health == 0 then
            if  not victim.death then
                victim.death = true
                victim.OnDeath:Trigger(victim)
            end
            return damage
        end

        victim.OnAttacked:Trigger(attacker,damage,buff)
        CardGameEntity.OnAttacked:Trigger(attacker,victim,damage,buff)
        if buff then
            victim:AddBuff(buff)
        end
        return damage
    end

    function CardGameEntity.AddBuff(entity,buffs)
        local addBuffs={}
        local index=1
        while buffs.id and buffs.id>0 do
            local temp=buffs.id%2
            buffs.id=math.floor(buffs.id/2)
            if temp==1 then
                table.insert(addBuffs,CardGameBuff:Create(BuffEnumMap[index],buffs.layer))
            end
            index=index+1
        end

        for i = 1, #addBuffs do
            local buff=addBuffs[i]
            if entity.buffs[buff.id] then
                local cardGameBuff=entity.buffs[buff.id]
                cardGameBuff:AddLayer(buff.layer)
                print("CardGameEntity:AddBuff:"..cardGameBuff.id)
            else
                entity.buffs[buff.id] = buff
                local cardGameBuff=entity.buffs[buff.id]
                print("CardGameEntity:AddBuff2:"..cardGameBuff.id)
                print("CardGameEntity:AddBuffLayer2:"..cardGameBuff.layer)
            end
        end
        return addBuffs
    end
    function CardGameEntity.ClearBuff(entity,buffsId)
        local clearBuffsId ={}
        local index=1
        while buffsId and buffsId>0 do
            local temp=buffsId%2
            buffsId=math.floor(buffsId/2)
            if temp==1 then
                local buffId = BuffEnumMap[index]
                print("CardGameEntity.ClearBuff"..buffId)
                if entity:GetBuff(buffId) then
                    table.insert(clearBuffsId,buffId)
                    entity.buffs[buffId] = nil
                end
            end
            index=index+1
        end
        return clearBuffsId
    end
    function CardGameEntity.RemoveBuffLayer(entity)
        local removeList={}
        for buffId,buff in pairs(entity.buffs) do
            local cardGameBuff = buff
            if cardGameBuff then
                cardGameBuff:RemoveLayer()
            end
            if cardGameBuff.layer==0 then
                table.insert(removeList,buffId)
            end
        end
        for k,v in pairs(removeList) do
            entity.buffs[k]=nil
        end
    end
    function CardGameEntity.RemoveAllBuff(entity)
        local removeList={}
        for buffId,buff in pairs(entity.buffs) do
            local cardGameBuff = buff
            if cardGameBuff then
                cardGameBuff:Remove()
            end
            if cardGameBuff.layer==0 then
                table.insert(removeList,buffId)
            end
        end
        for k,v in pairs(removeList) do
            entity.buffs[k]=nil
        end
    end
    function CardGameEntity.GetBuff(entity,buffId)
        if entity.buffs[buffId] and entity.buffs[buffId].layer==0 then
            entity.buffs[buffId] = nil
            return nil
        end
        return entity.buffs[buffId] or nil
    end
    function CardGameEntity.BuffToString(entity,buffId)
        local cardGameBuff = CardGameEntity.GetBuff(entity,buffId)
        return cardGameBuff:ToString()
    end
    CardGameMonster={
        name="",
        health=0,
        maxHealth=0,
        attack=0,
        attackBuff = nil,
        nextAttackBuff = nil,
        --攻击次数
        attackCount = 1,
        armor=0,
        nextArmor = 0,
        isPlayer = false,
        death = false,
        index = 0
    }
    function CardGameMonster:IsMonster()
        return true
    end
    function CardGameMonster:IsPlayer()
        return false
    end
    function CardGameMonster:Create(monsterName)
        local cardGameMonster = {}
        setmetatable(cardGameMonster,self)
        self.__index = self
        cardGameMonster.name = monsterName
        cardGameMonster.buffs = {}
        cardGameMonster.OnDeath = REGISTER:New()
        cardGameMonster.OnSetAction = REGISTER:New()
        cardGameMonster.OnBeforeTakeDamage = REGISTER:New()
        cardGameMonster.OnAttacked = REGISTER:New()
        cardGameMonster.OnTakeDamage = REGISTER:New()
        cardGameMonster.OnTakeDamageAni = REGISTER:New()
        cardGameMonster.death = false
        cardGameMonster.OnDeath:Register(function(cardGameMonster)
            cardGameMonster.attackBuff = nil
            cardGameMonster.death = true
        end)
        cardGameMonster.OnBeforeTakeDamage:Register(function(attacker,victim)
            return true
        end)
        return cardGameMonster
    end

    function CardGameMonster:StorageAction(cardGamePlayer,roundNum,level)
        --存储下一个行动(未使用)
        self.OnSetAction:Trigger(cardGamePlayer,roundNum,level)
        --if self:GetBuff(BuffEnum.Week) and  self.attack>0 then
        --    self.attack= CardGameBuff.Week(self.attack)
        --end
        --if cardGamePlayer:GetBuff(BuffEnum.Hurt) then
        --    self.attack = CardGameBuff.Hurt(self.attack)
        --end
    end
    function CardGameMonster:TakeDamage(cardGamePlayer)
        if not self.OnBeforeTakeDamage:Trigger(self,cardGamePlayer) then return end
        --同步ui
        CardGameEntity.TakeDamage(self,cardGamePlayer)
    end
    function  CardGameMonster:Clear()
        if not self.death then
            self.OnDeath:Trigger(self)
        end
    end

    function CardGameMonster:Attacked(cardGamePlayer,damage,buff)
        return CardGameEntity.Attacked(self,cardGamePlayer,damage,buff)
    end

    function CardGameMonster:AddBuff(buff)
        if buff.id==BuffEnum.Default then return end
        if buff.id ==BuffEnum.Week and not self:GetBuff(BuffEnum.Week) then
            self.attack= CardGameBuff.Week(self.attack)
        end
        CardGameEntity.AddBuff(self,buff)
    end
    function CardGameMonster:RemoveBuffLayer()
        CardGameEntity.RemoveBuffLayer(self)
    end
    function CardGameMonster:GetBuff(buffId)
        print("CardGameMonster:GetBuff"..buffId)
        return CardGameEntity.GetBuff(self,buffId)
    end
    function CardGameMonster:BuffToString(buffId)
        return CardGameEntity.BuffToString(self,buffId)
    end
    function CardGameMonster:ToString()
        local attackBuffId = 0
        if self.attackBuff then
            attackBuffId = self.attackBuff.id
        end
        return table.concat({
            self.name,
            self.health,
            self.maxHealth,
            self.attack,
            self.armor,
            self.nextArmor,
            attackBuffId,
            self.attackCount,
            self.index
        }, ",") -- 直接连接每个怪兽的属性
    end
    CardGamePlayer={
        cost = 3,
        maxCost = 3,
        --局中额外Cost,每局清0
        extraCost = 0,
        health = 74,
        hasTarget = false,
        targetIndex = 1,
        isPlayer = true,
        attack = 0,
        attackCount = 1,
        treasures = {},
        --开始选择牌
        startChoice = false,
        user={},
        death=false,
    }
    function CardGamePlayer:GetMaxCost()
        return self.maxCost + self.extraCost
    end
    function CardGamePlayer:AddCoin(coin)
        self.player.coin =  self.player.coin + coin
    end
    function CardGamePlayer:GetCoin()
        return self.player.coin
    end
    function CardGamePlayer:RoundStart()
        local maxCost = self:GetMaxCost()
        self.cost= math.min(self.cost + maxCost,maxCost)
        self.armor = 0
        self.player.armor = self.armor
        self:RemoveBuffLayer()
    end
    function CardGamePlayer:HasTarget(hasTarget)
        self.hasTarget = hasTarget
    end
    function CardGamePlayer:Create(player,occupationEnum)
        local cardGamePlayer = {}
        setmetatable(cardGamePlayer,self)
        self.__index = self
        cardGamePlayer.CardKnapsack = CardKnapsack:Create(occupationEnum)
        cardGamePlayer.HandCard = HandCard:Create()
        cardGamePlayer.RoundCard = RoundCard:Create()
        cardGamePlayer.index = player.index
        cardGamePlayer.name = player.name
        cardGamePlayer.player = player
        cardGamePlayer.health = player.health
        cardGamePlayer.maxHealth = player.maxhealth
        cardGamePlayer.armor = player.armor
        cardGamePlayer.buffs = {}
        cardGamePlayer.treasures = {}
        cardGamePlayer.user = {}
        cardGamePlayer.OnBeforeTakeDamage = REGISTER:New()
        cardGamePlayer.OnAttacked = REGISTER:New()
        cardGamePlayer.OnDeath = REGISTER:New()
        cardGamePlayer.OnTakeDamage = REGISTER:New()
        cardGamePlayer.death =false
        --播放动画用
        cardGamePlayer.OnTakeDamageAni = REGISTER:New()
        cardGamePlayer.OnBeforeTakeDamage:Register(function(attacker,victim)
            return true
        end)
        cardGamePlayer.OnTakeDamage:Register(function()

        end)
        return cardGamePlayer
    end
    function CardGamePlayer:Attacked(killer,damage,buff)
        if self.death==true then
            return
        end
        CardGameEntity.Attacked(self,killer,damage,buff)
        if damage<0 then
            self:SignalPlus("PlayerRecovery")
        end
        if damage>0 then
            self:SignalPlus("PlayerAttacked")
        end
        self.player.health = self.health
        self.player.armor = self.armor
    end
    function CardGamePlayer:IsMonster()
        return false
    end
    function CardGamePlayer:IsPlayer()
        return true
    end
    function CardGamePlayer:TakeDamage(cardGameEntity, damage, buff)
        self.attack = damage
        if buff then
            self.attackBuff = buff
        end
        if not self.OnBeforeTakeDamage:Trigger(self,cardGameEntity) then return  end
        CardGameEntity.TakeDamage(self,cardGameEntity)
    end
    function CardGamePlayer:AddHealth(value)
        self.health = math.min(self.maxHealth,self.health + value)
        self.player.health = self.health
        self:SignalPlus("PlayerRecovery")
    end
    function CardGamePlayer:AddArmor(armorValue)
        self.armor = self.armor + armorValue
        if armorValue>=999 then
            self.armor = 999
        end
        self.player.armor = self.armor
        self:SignalPlus("PlayerAddArmor")
    end
    function CardGamePlayer:SignalPlus(msg)
        self.player:SignalPlus(msg)
    end
    function CardGamePlayer:SelectTarget(controlEnum,cardGameMonsters)
        if controlEnum == ControlEnum.Left then
            if  self.targetIndex <= 1 then
                self.targetIndex = 1
            else
                self.targetIndex = self.targetIndex - 1
                if self.targetIndex > 0 then
                    if cardGameMonsters:GetByIndex(self.targetIndex).health==0 then
                        self:SelectTarget(controlEnum,cardGameMonsters)
                    end
                end
            end
        end
        if controlEnum == ControlEnum.Right then
            if  self.targetIndex >=cardGameMonsters:Count() then
                self.targetIndex = cardGameMonsters:Count()
            else
                self.targetIndex = self.targetIndex + 1
                if self.targetIndex < cardGameMonsters:Count() then
                    if cardGameMonsters:GetByIndex(self.targetIndex).health==0 then
                        self:SelectTarget(controlEnum,cardGameMonsters)
                    end
                end
            end
        end
        print("self.targetIndex")
        print(self.targetIndex)
    end

    function CardGamePlayer:GetTargetIndex(cardGameMonsters)
        if cardGameMonsters:GetByIndex(self.targetIndex).health==0 then
            for i = 1, cardGameMonsters:Count() do
                if cardGameMonsters:GetByIndex(i).health~=0 then
                    self.targetIndex = i
                end
            end
        end
        return self.targetIndex
    end
    function CardGamePlayer:AddBuff(buff)
        if buff.id==BuffEnum.Default then return end
        print("CardGamePlayer:AddBuff"..buff.id..","..buff.layer)
        local buffs=CardGameEntity.AddBuff(self,buff)
        for k,v in pairs(buffs) do
            local playerBuff = self:GetBuff(v.id)
            self:SignalPlus("AddPlayerBuff"..playerBuff:ToString())
        end
    end
    function CardGamePlayer:ClearBuff(buff)
        if buff.id==BuffEnum.Default then return end
        local buffIds = CardGameEntity.ClearBuff(self,buff.id)
        for k,v in pairs(buffIds) do
            self:SignalPlus("ClearPlayerBuff,"..v)
        end
    end
    function CardGamePlayer:RemoveBuffLayer()
        CardGameEntity.RemoveBuffLayer(self)
    end
    function CardGamePlayer:RemoveAllBuff()
        CardGameEntity.RemoveAllBuff(self)
    end
    function CardGamePlayer:GetBuff(buffId)
        print("CardGamePlayer:GetBuff2:")
        print(buffId)
        return CardGameEntity.GetBuff(self,buffId)
    end
    function CardGamePlayer:BuffToString(buffId)
        return CardGameEntity.BuffToString(self,buffId)
    end
    function CardGamePlayer:AddTreasure(treasure)
        self.treasures[treasure.id] = treasure
    end
    function CardGamePlayer:GetTreasure(treasureId)
        return self.treasures[treasureId]
    end
    function CardGamePlayer:IncreaseTreasureLayer(treasureId)
        local treasure = self:GetTreasure(treasureId)
        if not treasure then return end
        treasure.layer =  treasure.layer + 1
    end
    function CardGamePlayer:DecreaseTreasureLayer(treasureId)
        local treasure = self:GetTreasure(treasureId)
        if not treasure then return end
        treasure.layer =  treasure.layer - 1
    end
    --CardGameTreasure 宝物
    CardGameTreasure={}
    function CardGameTreasure:Create(treasureId)
        local cardGameTreasure = {}
        setmetatable(cardGameTreasure,self)
        self.__index = self
        cardGameTreasure.id = treasureId
        -- "接下来三回合怪物只有1血"
        if cardGameTreasure.id == 1 then
            cardGameTreasure.layer = 3
        end
        return cardGameTreasure
    end

    function Card:CreateCardGroup(occupationEnum)
        local cards = {}
        setmetatable(cards,self)
        self.__index = self
        if OccupationEnum.Soldier == occupationEnum then
            cards =Card:CreateSoldierCardGroup()
        end
        return cards
    end
    function Card:GetBuff()
        if self.buff.id ~=0 then
            return self.buff
        end
        return nil
    end
    function Card:Copy()
        local card = {}
        setmetatable(card,self)
        self.__index = self
        card.name = self.name
        card.cost = self.cost
        card.damage = self.damage
        card.armor = self.armor
        card.desc = self.desc
        card.id = self.id
        card.cost = self.cost
        card.damage = self.damage
        card.armor = self.armor
        card.buff  =  CardGameBuff:Create(self.buff.id,self.buff.layer)
        card.cardType = self.cardType
        card.cardTarget = self.cardTarget
        return card
    end
    function Card:ToString()
        local card={
            self.id,
            self.cost,
            self.damage,
            self.armor,
            self.buff.id,
            self.buff.layer,
            self.cardType,
            self.cardTarget,
            self.level,
            self.attackCount,
        }
        local rst=table.concat(card,',')
        return rst
    end

    CardKnapsack={
        maxCard = 9
    }
    --背包
    function CardKnapsack:AddCard(card)
        table.insert(self.cards,card)
    end
    function CardKnapsack:Create(occupationEnum)
        print("创建卡包")
        local cardKnapsack = {}
        setmetatable(cardKnapsack,self)
        self.__index = self
        self.cards = Card:CreateCardGroup(occupationEnum)
        return cardKnapsack
    end
    function CardKnapsack:RemoveCard(cardIndex)
        if cardIndex==0 then
            return
        end
        print("背包移除"..cardIndex)
        print("背包移除"..self.cards[cardIndex].name)
        table.remove(self.cards,cardIndex)
    end

    function CardKnapsack:LevelUpCard(cardIndex)
        if cardIndex==0 then
            return
        end
        print("背包升级"..cardIndex)
        print("背包升级"..self.cards[cardIndex].name)
        cardIndex = self.unLevelUpCardsIndex[cardIndex]
        local originCardId  = self.cards[cardIndex].id
        self.cards[cardIndex] = Card:Create(CardsConfig[originCardId+100])
    end
    function CardKnapsack:CopyAllRandomCards()
        local copyCards = {}
        for i = 1, #self.cards do
            copyCards[i] = self.cards[i]:Copy()
        end
        local index = 1
        local tab = {}
        while #copyCards~=0 do
            local n=math.random(0,#copyCards)
            if copyCards[n]~=nil then
                tab[index]=copyCards[n]
                table.remove(copyCards,n)
                index=index+1
            end
        end
        return tab
    end
    function CardKnapsack:ToString()
        local cards={}
        for i = 1, #self.cards do
            local card = self.cards[i]
            table.insert(cards,card.id)
            table.insert(cards,card.cost)
            table.insert(cards,card.damage)
            table.insert(cards,card.armor)
            table.insert(cards,card.cardType)
            table.insert(cards,card.attackCount)
        end
        self.toStringArgsCount = 6
        return table.concat(cards,',')
    end

    --将未升级的卡牌转换未String
    function CardKnapsack:UnLevelUpToString()
        local cards={}
        self.unLevelUpCardsIndex = {}
        for i = 1, #self.cards do
            local card = self.cards[i]
            if not card.isLevelUp then
                table.insert(cards,card.id)
                table.insert(cards,card.cost)
                table.insert(cards,card.damage)
                table.insert(cards,card.armor)
                table.insert(cards,card.cardType)
                table.insert(cards,card.attackCount)
                table.insert(self.unLevelUpCardsIndex,i)
            end
        end
        self.toStringArgsCount = 6
        return table.concat(cards,',')
    end
    HandCard = {
        cards ={},
        count = 0,
        curIndex = 1,
        --toWhere 2:移到不可用的牌 1:移到抽牌堆,0:移到弃牌堆
        toWhere = 0,
    }
    function HandCard:Create()
        print("创建手牌")
        local handCard = {}
        setmetatable(handCard,self)
        self.__index = self
        self.cards = Stack:New()
        self.onUseCard = REGISTER:New()
        self.onUsedCard = REGISTER:New()
        return handCard
    end
    function HandCard:GetByIndex(cardIndex)
        --local cards = self.cards:ToArray()
        --return cards[cardIndex]
        return self.cards:FindElementAtPosition(self.cards.count - cardIndex + 1)
    end
    function HandCard:SelectCard(controlEnum)
        if self.cards.count==0 then return end
        if controlEnum == ControlEnum.Left then
            self.curIndex = math.max(1, self.curIndex - 1)
        elseif controlEnum == ControlEnum.Right then
            self.curIndex = math.min(self.cards.count, self.curIndex + 1)
        end
    end

    function HandCard:GetCurCardIndex()
        self.curIndex = math.min(self.curIndex, self.cards.count)
        return self.curIndex
    end

    function HandCard:GetCurCard()
        if not self:GetCurCardIndex() then print("HandCard:GetCurCard index is zero") return end
        if not self.cards  then return print("HandCard.cards is nil") end
        local cardIndex = self:GetCurCardIndex()
        local cardCard=self:GetByIndex(cardIndex)
        if not cardCard then print("HandCard:GetCurCard error index:"..cardIndex) return end
        return cardCard
    end
    function HandCard:AddNewCard(cardGamePlayer,card)
        if not card then return end
        if self.cards.count>=10  then print("超出手牌上限") return end
        self.cards:Push(card)
        self.count = self.cards.count
        Card.OnAddCard:Trigger(cardGamePlayer,card:Copy(),card)
    end
    function HandCard:AddCard(cardGamePlayer)
        local roundCard = cardGamePlayer.RoundCard
        if not roundCard.DrawPiles  then print("DrawPiles未初始化") return  end
        if self.cards.count>=10  then print("超出手牌上限") return  end
        if roundCard.DrawPiles.count==0 then
            roundCard:DiscardPile2DrawPiles()
        end
        local drawPile=roundCard.DrawPiles:Pop()
        if drawPile==nil then print("洗牌后没有牌")  return end
        print("HandCard:AddCard,name:"..drawPile.name)
        print("HandCard:AddCard,cost:"..drawPile.cost)
        self.cards:Push(drawPile)
        self.count = self.cards.count
        PrintStackCard(self.cards)
        Card.OnAddCard:Trigger(cardGamePlayer,drawPile:Copy(),drawPile)
    end

    function HandCard:RemoveCards(roundCard)
        while(self.cards.count>0) do
            roundCard.DiscardPile:Push(self.cards:Pop())
        end
    end

    function HandCard:RemoveCardByIndex(removeCardIndex)

        --table.remove(cards,removeCardIndex)
        --self.cards = Stack:New(cards)
        --牌序    |1|2|3|4|5(top)|
        --栈编号  |5|4|3|2|1(top)|
        --移除第4张时，对应栈中的2号位置
        --所以RemoveAtPosition参数填写 stack.count - removeCardIndex + 1
        self.cards:RemoveAtPosition(self.cards.count-removeCardIndex+1)
    end

    function PrintStackCard(cards)
        cards = cards:ToArray()
        if  not cards  then return end
        for i = 1, #cards do
            if cards[i] then
                print(i..":"..cards[i].name)
            end
        end
    end
    function HandCard:UseCard(cardGamePlayer)
        local roundCard = cardGamePlayer.RoundCard
        local cardIndex = self:GetCurCardIndex()
        if cardIndex == 0 then print("HandCard:UseCard,curIndex is zero") return false end
        local useCard = self:GetCurCard()
        local copyUseCard=useCard:Copy()

        if cardGamePlayer:GetBuff(BuffEnum.Loan) then
            if cardGamePlayer.cost-useCard.cost <  - cardGamePlayer:GetMaxCost()*4 then
                return false
            end
        elseif useCard.cost > cardGamePlayer.cost then
            return false
        end
        self.onUseCard:Trigger(cardGamePlayer,copyUseCard,useCard)
        if copyUseCard.cardType==CardTypeEnum.Deplete then
            roundCard.DeathCards:Push(useCard)
        else
            roundCard.DiscardPile:Push(useCard)
        end
        self.onUsedCard:Trigger(cardGamePlayer,copyUseCard,useCard)
        self:RemoveCardByIndex(cardIndex)
        self.count = self.cards.count
        return true
    end
    --小局游戏内卡组
    RoundCard={
        --抽牌堆
        DrawPiles = nil,
        --弃牌堆
        DiscardPile = nil,
        --不可用的牌
        DeathCards = nil
    }
    function RoundCard:Create()
        print("创建回合牌组")
        local roundCard = {}
        setmetatable(roundCard,self)
        self.__index = self
        self.card = nil
        return roundCard
    end
    function RoundCard:Init(cardKnapsack)
        self.DrawPiles = Stack:New(cardKnapsack:CopyAllRandomCards())
        self.DiscardPile = Stack:New()
        self.DeathCards = Stack:New()
    end
    function RoundCard:DiscardPile2DrawPiles()
        print("========洗牌========")
        local tmp = {}
        local index = 1
        while self.DiscardPile.count~=0 do
            tmp[index] = self.DiscardPile:Pop()
            index=index+1
        end
        while #tmp~=0 do
            local n=math.random(1,#tmp)
            if tmp[n]~=nil then
                self.DrawPiles:Push(tmp[n])
                table.remove(tmp,n)
            end
        end
    end


end

if UI then
    CardsUI={
        cardsBox ={},
        cardsPos={},
        cardsCost = {},
        cardsArmor = {},
        cardsDamage= {},
        cards={},
        monsters={},
        isSelect = false,
        cardNum = 0,
        curHandCardIndex = 0,
        targetIndex = 1,
        targetOffsetY = 50,
        cardWidth = 100,
        cardHeight = 175,
        damageOffset = Vector2:New(0,175-40),
        armorOffset = Vector2:New(100-30-35,175-40),
        --手牌左右间隔
        cardSpan = 100,
        descBGHeight = 200,
        descBG = UI.Box.Create()
    }
    function CardsUI:Create(screenSize)
        local cardsUI = {}
        setmetatable(cardsUI,self)
        self.__index = self
        --目标位置左右间隔
        self.cardTargetSpan = 300
        self.screenWidth = screenSize.width
        self.screenHeight = screenSize.height
        self.startY = self.screenHeight -  self.cardHeight - 40
        self.targetPosStartY = 200
        cardsUI.cardsPos = {}
        cardsUI.cardsUI = {}
        cardsUI.cards = {}

        return cardsUI
    end

    function CardsUI:SetPos(cardNum)
        self.cardNum = cardNum
        local length = self.cardWidth*cardNum  -  (self.cardWidth - self.cardSpan)*(cardNum -1)
        local startPos = (self.screenWidth-length)/2
        for i = 1, cardNum  do
            self.cardsPos[i]={x=startPos+(i-1)*self.cardSpan ,y=self.startY}
        end
    end

    function CardsUI:UpdatePos()
        for i = 1, self.cardNum  do
            local ui=self.cardsUI[i]
            if ui then
                local cardInfo= ui.cardsBox:Get()
                local startPos ={x= cardInfo.x,y=cardInfo.y}
                local endPos ={x= self.cardsPos[i].x,y=self.startY}
                self:DoMove(ui,startPos,endPos)
            end
        end
    end

    function CardsUI:OnRoundEnd()
        self:RemoveCards()
        self.hasTarget = nil
        self.curHandCardIndex = 0
    end

    function CardsUI:RemoveCards()
        print(" CardsUI:RemoveCards")
        for i = 1, #self.cardsUI  do
            Invoke(function()
                self:RemoveCard(i)
            end,0.1*i)
        end
    end
    function CardsUI:DoColorAlpha(ui,time)
        local origin = ui.cardsBox:Get()
        ui.cardsBox:DoColor({r=origin.r,g=origin.g,b=origin.b,a=255}, {r=origin.r,g=origin.g,b=origin.b,a=0},time)
        origin = ui.cardsCost:Get()
        ui.cardsCost:DoColor({r=origin.r,g=origin.g,b=origin.b,a=255},  {r=origin.r,g=origin.g,b=origin.b,a=0},time)
        origin = ui.cardsDamage:Get()
        ui.cardsDamage:DoColor({r=origin.r,g=origin.g,b=origin.b,a=255},  {r=origin.r,g=origin.g,b=origin.b,a=0},time)
        origin = ui.cardsArmor:Get()
        ui.cardsArmor:DoColor({r=origin.r,g=origin.g,b=origin.b,a=255},  {r=origin.r,g=origin.g,b=origin.b,a=0},time)
    end
    function CardsUI:DoMove(ui,startPos,endPos,time)
        if ui.choice then
            return
        end
        time = time or 0.5
        ui.cardsBox:Set(startPos)
        ui.cardsBox:Show()
        ui.cardsCost:Show()
        ui.cardsDamage:Show()
        ui.cardsArmor:Show()
        ui.cardsBox:DoMove(startPos, endPos,time)
        ui.cardsCost:DoMove(startPos, endPos,time)
        ui.cardsDamage:DoMove(startPos+self.damageOffset, endPos +self.damageOffset,time)
        ui.cardsArmor:DoMove(startPos+self.armorOffset, endPos +self.armorOffset,time)
    end
    function CardsUI:SetCard(card,cardIndex)
        self.curHandCardIndex = cardIndex
        self.cards[cardIndex] = card
    end
    function CardsUI:AddCard(card,cardIndex)
        local endPos = self.cardsPos[cardIndex]
        local startPos =Vector2:New({x=0,y= self.screenHeight})
        self.cardsUI[cardIndex]=self:CreateCardUI()
        self.cards[cardIndex] = card
        self:SetCardUI(self.cardsUI[cardIndex],card,startPos)
        local ui = self.cardsUI[cardIndex]
        self:DoMove(ui,startPos,endPos)
    end
    function CardsUI:CreateCardUI()
        return{
            cardsBox=UI.Box.Create(),
            cardsCost=UI.Text.Create(),
            cardsArmor=UI.Text.Create(),
            cardsDamage=UI.Text.Create(),
            --是否被选中
            choice = false
        }
    end
    function CardsUI:Hide(cardUI)
        cardUI.cardsBox:Hide()
        cardUI.cardsCost:Hide()
        cardUI.cardsDamage:Hide()
        cardUI.cardsArmor:Hide()
    end
    function CardsUI:SetCardUI(cardUI,card,pos)
        cardUI.cardsBox:Show()
        cardUI.cardsCost:Show()
        cardUI.cardsDamage:Show()
        cardUI.cardsArmor:Show()
        cardUI.cardsBox:Set({ width=self.cardWidth, height=self.cardHeight})
        if card.cardType==CardTypeEnum.Attack then
            cardUI.cardsBox:Set(CardsColor[1])
        end
        if card.cardType==CardTypeEnum.Skill then
            cardUI.cardsBox:Set(CardsColor[2])
        end
        if card.cardType==CardTypeEnum.Deplete then
            cardUI.cardsBox:Set(CardsColor[3])
        end
        cardUI.cardsCost:Set({ text=tostring(card.cost) , font="medium", align="left", r=255, g=255, b=255, width=100, height=60})
        local damage = card.damage
        if card.attackCount >1 then
            damage = tostring(damage.."*"..card.attackCount)
        end
        cardUI.cardsDamage:Set({ text=tostring(damage) , font="medium", align="left", r=255, g=255, b=255, width=100, height=60})
        cardUI.cardsArmor:Set({ text=tostring(card.armor) , font="medium", align="center", r=255, g=255, b=255, width=100, height=60})
        self:SetCardUIPos(cardUI,pos)
    end
    function CardsUI:SetCardUIPos(cardUI,pos)
        pos = Vector2:New(pos)
        cardUI.cardsBox:Set(pos)
        cardUI.cardsCost:Set(pos)
        cardUI.cardsDamage:Set(pos+self.damageOffset)
        cardUI.cardsArmor:Set(pos+self.armorOffset)
    end
    function CardsUI:RemoveCard(cardIndex)
        if not self.cardsUI[cardIndex] then return end
        local ui=self.cardsUI[cardIndex]
        local cardInfo =  ui.cardsBox:Get()
        local startPos = {x=cardInfo.x,y= cardInfo.y}
        local endPos = {x=self.screenWidth,y= self.screenHeight}
        self:UnSelectTarget()
        self:DoMove(ui,startPos,endPos)
    end
    function CardsUI:SelectCard(cardIndex)
        if self.hasTarget then return end
        for i = 1, self.cardNum  do
            local ui=self.cardsUI[i]
            if ui and ui.cardsBox:Get().y~=self.startY then
                local cardInfo= ui.cardsBox:Get()
                local startPos ={x= cardInfo.x,y=cardInfo.y}
                local endPos ={x= self.cardsPos[i].x,y=self.startY}
                self:DoMove(ui,startPos,endPos)
            end
        end
        local ui=self.cardsUI[cardIndex]
        local cardInfo= ui.cardsBox:Get()
        local startPos ={x= cardInfo.x,y=cardInfo.y}
        local endPos ={x= self.cardsPos[cardIndex].x,y=self.startY-30}
        self.curHandCardIndex = cardIndex
        self:DoMove(ui,startPos,endPos)
        local set =ui.cardsBox:Get()
        ui.cardsBox:DoColor({r=255,g=255,b=255},{r=set.r,g=set.g,b=set.b},1)
    end
    function CardsUI:ShowDesc(card,targetsUI,targetIndex,playerUI)
        self.screenHeight = self.screenHeight or UI.ScreenSize().height
        self.screenWidth = self.screenWidth or UI.ScreenSize().width
        --描述位置
        self.nameStartY  = self.nameStartY  or  self.screenHeight/2-50
        self.typeStartY  = self.typeStartY or self.screenHeight/2
        self.descStartY  = self.descStartY or self.screenHeight/2+50
        if self.descBG then
            self.descBG:Set({x=0,y= self.nameStartY-30,width=self.screenWidth,height=self.descBGHeight,r=0,g=0,b=0,a=110})
            self.descBG:Show()
        end
        self.cardName = DrawText:Create()
        self.cardDesc = DrawText:Create()
        self.cardType = DrawText:Create()
        local damageDesc
        if  card.attackCount==1 then
            damageDesc = string.format(CardsDesc[card.id][2],card.damage)
        else
            damageDesc = string.format(CardsDesc[card.id][2],card.damage,card.attackCount)
        end
        local cardTypeName = CardType[card.cardType]
        self.cardName:Set({text =CardsDesc[card.id][1],x=self.screenWidth/2,y=self.nameStartY,size=4})
        self.cardType:Set({text =cardTypeName,x=self.screenWidth/2,y=self.typeStartY,size=2})
        local damage = card.damage
        if  playerUI and playerUI:GetBuff(BuffEnum.Week)>0 and damage>0 then
            damage = CardGameBuff.Week(damage)
        end
        if targetIndex then
            if targetsUI:GetBuff(targetIndex,BuffEnum.Hurt)>0 and card.damage>0 then
                damage = CardGameBuff.Hurt(damage)
            end
            if  card.attackCount==1 then
                damageDesc = string.format(CardsDesc[card.id][2],card.damage)
            else
                damageDesc = string.format(CardsDesc[card.id][2],card.damage,card.attackCount)
            end
        end
        if card.damage~=0 then
            self.cardDesc:Set({text =damageDesc,x=self.screenWidth/2,y=self.descStartY,size=3,kerning=2})
        elseif card.armor>0 then
            local armorDesc = string.format(CardsDesc[card.id][2],card.armor)
            self.cardDesc:Set({text =armorDesc,x=self.screenWidth/2,y=self.descStartY,size=3,kerning=2})
        else
            self.cardDesc:Set({text =CardsDesc[card.id][2],x=self.screenWidth/2,y=self.descStartY,size=3,kerning=2})
        end
        self.cardName:Adaptation("center")
        self.cardDesc:Adaptation("center")
        self.cardType:Adaptation("center")
    end
    function CardsUI:HideDesc()
        self.cardName = nil
        self.cardDesc = nil
        self.cardType = nil
        if self.descBG then
            self.descBG:Hide()
        end
        collectgarbage()
    end
    function CardsUI:SelectTarget(targetsUI,targetIndex,playerUI)
        if targetIndex==0 then print("1003 error:not targetIndex") return end
        if self.curHandCardIndex==0 then print("1003 error:curHandCardIndex is 0") return end
        local ui=self.cardsUI[self.curHandCardIndex]
        local cardInfo= ui.cardsBox:Get()
        local startPos ={x= cardInfo.x,y=cardInfo.y}
        self.hasTarget = true
        local targetPos = targetsUI.targetsPos[targetIndex]
        local newPos = {}
        newPos.y= targetPos.y + self.targetOffsetY
        --血条一定比牌宽，居中牌相对血条的位置
        newPos.x = (targetsUI.width - self.cardWidth)/2 + targetPos.x
        local curCard =  self.cards[self.curHandCardIndex]
        --技能或者群攻设置中央
        if  curCard.cardTarget==CardTargetEnum.Multiple or curCard.cardTarget==CardTargetEnum.Self then
            newPos = {x=(self.screenWidth-self.cardWidth)/2,y=newPos.y}
        end
        if curCard.cardType==CardTypeEnum.Attack or curCard.cardType==CardTypeEnum.Deplete then
            --群攻1
            if curCard.cardTarget==CardTargetEnum.Multiple then
                targetsUI:SelectedAll()
            elseif curCard.cardTarget==CardTargetEnum.Single then
                targetsUI:Selected(targetIndex)
            elseif curCard.cardTarget==CardTargetEnum.Self then
                targetsUI:SelectedSelf()
            end
        end


        --根据buffId修改卡的伤害
        local card =  self.cards[self.curHandCardIndex]
        local damage = card.damage
        print("buff targetIndex"..targetIndex)
        if  playerUI:GetBuff(BuffEnum.Week)>0 and damage>0 then
            damage = CardGameBuff.Week(damage)
        end
        if targetsUI:GetBuff(targetIndex,BuffEnum.Hurt)>0 and damage>0 then
            damage = CardGameBuff.Hurt(damage)
        end
        --无增伤为白色
        ui.cardsDamage:Set({r=255, g=255, b=255})
        --大于原本卡面的攻击显示为绿色
        if damage>CardsConfig[card.id][3] then
            ui.cardsDamage:Set({r=0, g=255, b=0})
        end
        --小于原本卡面的攻击显示为红色
        if damage<CardsConfig[card.id][3] then
            ui.cardsDamage:Set({r=255, g=0, b=0})
        end
        if card.attackCount > 1 then
            damage = tostring(damage.."*"..card.attackCount)
        end
        ui.cardsDamage:Set({ text=tostring(damage)})

        self:DoMove(ui,startPos,newPos)
    end
    function CardsUI:UnSelectTarget()
        if not self.hasTarget then return end
        if self.curHandCardIndex==0 then print("1000 error:self.curHandCardIndex==0")  return end
        local ui = self.cardsUI[self.curHandCardIndex]
        local cardInfo= ui.cardsBox:Get()
        local startPos ={x= cardInfo.x,y=cardInfo.y}
        local endPos ={x= self.cardsPos[self.curHandCardIndex].x,y=self.startY-30}
        local card = self.cards[self.curHandCardIndex]
        local damage = card.damage
        if card.attackCount>1 then
            damage = tostring(damage.."*"..card.attackCount)
        end
        ui.cardsDamage:Set({ text=tostring(damage),r=255, g=255, b=255})
        self.hasTarget = false
        self:DoMove(ui,startPos,endPos)
    end
    --toWhere 2:移到不可用的牌 1:移到抽牌堆,0:移到弃牌堆(动画)
    function CardsUI:UseCard(index,toWhere)
        if  self.cardNum==0 then return end
        if  index==0 then return end
        local ui = self.cardsUI[index]
        local cardInfo= ui.cardsBox:Get()
        local startPos ={x= cardInfo.x,y=cardInfo.y}
        local endPos = {x=self.screenWidth,y= self.screenHeight+10}
        local card =  self.cards[self.curHandCardIndex]
        if toWhere == 1 then
            endPos = {x=0,y=self.screenHeight+10}
        elseif toWhere == 2 then
            card.cardType=CardTypeEnum.Deplete
        end

        if card.cardType==CardTypeEnum.Deplete then
            self:DoColorAlpha(ui,3)
        else
            self:DoMove(ui,startPos,endPos)
        end
        table.remove(self.cardsUI,index)
        self.hasTarget = false
        collectgarbage()
    end

    function CardsUI:GetCardUI(index)
       return self.cardsUI[index]
    end



    TargetsUI = {
        startY = 50,
        --目标血条缓冲/背景色/被选中颜色
        color = {{r=255, g=0, b=0,a=255},{r=128, g=128, b=128,a=255},{r=255,g=165,b=0,a=255}},
        height = 10,
        width = 200,
        span = 300,
        buffSpan = 25,
        --ui
        targetsPos ={},
        targetsBox={},
        targetsText={},
        targetsPic={},
        targetBuff={},
        --实体
        targets={},
    }

    --CardGameMonster:ToString()的顺序
    function TargetsUI:SetMonsters(args,monsterIndex,playerUI)
        local i  = monsterIndex
        self.targets[i] =  self.targets[i] or {}
        local monster = self.targets[i]
        self.targetBuff[i] =  self.targetBuff[i] or {}
        monster.name=args[1]
        monster.health=tonumber(args[2])
        monster.maxHealth=tonumber(args[3])
        monster.attack=tonumber(args[4])
        monster.armor=tonumber(args[5])
        monster.nextArmor=tonumber(args[6])
        monster.attackBuffId =tonumber(args[7])
        monster.attackCount=tonumber(args[8])
        monster.buffs = monster.buffs or {}

        local originWidth=self.targetsBox[i].BG:Get().width-4
        self.targetsBox[i].FG:Set({width = monster.health/monster.maxHealth*originWidth})
        self.targetsText[i].healthMaxHealth:Set({text=monster.health.."/"..monster.maxHealth})

        local damage = monster.attack
        local armor = monster.armor

        if monster.attackBuffId ~=0 then
            self.targetsPic[i].attackBuffPic:Show()
        else
            self.targetsPic[i].attackBuffPic:Hide()
        end

        if damage>0 then
            if playerUI:GetBuff(BuffEnum.Week)>0 and  damage>0 then
                damage= CardGameBuff.Week(damage)
            end
            if playerUI:GetBuff(BuffEnum.Hurt)>0 then
                damage = CardGameBuff.Hurt(damage)
                print("重伤增伤UI")
            end
            self.targetsPic[i].attackPic:Set({picture="attack_1"})
            self.targetsPic[i].attackPic:Show()
            if monster.attackCount>1 then
                self.targetsText[i].attack:Set({text=tostring(damage).."*"..tostring(monster.attackCount)})
            else
                self.targetsText[i].attack:Set({text=tostring(damage)})
            end
            self.targetsText[i].attack:Show()
        else
            self.targetsText[i].attack:Hide()
            self.targetsPic[i].attackPic:Hide()
        end

        if monster.nextArmor>0 then
            self.targetsPic[i].nextArmorPic:Show()
        else
            self.targetsPic[i].nextArmorPic:Hide()
        end

        if monster.armor>0 then
            self.targetsPic[i].armorPic:Set({picture="armor"})
            self.targetsText[i].armor:Show()
        else
            self.targetsPic[i].armorPic:Hide()
            self.targetsText[i].armor:Hide()
        end

        if armor>0 then
            self.targetsText[i].armor:Set({text=tostring(armor)})
        end

        if monster.health==0 then
            self:Hide(i)
        end

    end
    function TargetsUI:GetBuff(monsterIndex,buffId)
        return self.targets[monsterIndex].buffs[buffId] or 0
    end
    function TargetsUI:SetBuff(args,monsterIndex)
        if not self.targets[monsterIndex] then return end
        local buffId = tonumber(args[1])
        local buffLayer = tonumber(args[2])
        if buffLayer==0 then
            return
        end
        print("SetMonstersBuff"..buffId..","..buffLayer)
        self.targets[monsterIndex].buffs[buffId] = buffLayer
        local pos =  self.targetsPos[monsterIndex]
        local picName = "hurt"
        if buffId==BuffEnum.Hurt then
            picName = "hurt"
        end
        if buffId==BuffEnum.Week then
            picName = "week"
        end
        if buffId==BuffEnum.Poison then
            picName = "poison"
        end
        if buffId==BuffEnum.SelfHarm then
            picName = "selfHarm"
        end
        if buffId==BuffEnum.Transfer then
            picName = "transfer"
        end
        if buffId==BuffEnum.Recovery then
            picName = "recovery"
        end
        if buffId==BuffEnum.DamageConservation then
            picName = "damageConservation"
        end
        if buffId==BuffEnum.Reclaim then
            picName = "reclaim"
        end
        if buffId==BuffEnum.Loan then
            picName = "loan"
        end
        if buffId==BuffEnum.ThornMail then
            picName = "thornMail"
        end
        if buffId==BuffEnum.Wounds then
            picName = "wounds"
        end
        if buffId==BuffEnum.SegmentShoot then
            picName = "segmentShoot"
        end
        local hasBuff = false
        for i = 1, #self.targetBuff[monsterIndex] do
            if self.targetBuff[monsterIndex][i].buffId==buffId then
                self.targetBuff[monsterIndex][i].text:Set({text=tostring(buffLayer)})
                hasBuff = true
            end
        end
        --已经存在buff图标不再添加图标而是堆叠数字
        if not hasBuff then
            local buffCount = #self.targetBuff[monsterIndex] + 1
            local buff={
                picName = picName,
                index = buffCount,
                buffId = buffId,
                pic=DrawPicture:Create(),
                text=UI.Text.Create()
            }
            buff.pic:Set({picture=picName,x=pos.x+(buffCount-1)*self.buffSpan,y=pos.y-24,width=17,height=17})
            buff.text:Set({text=tostring(buffLayer),x=pos.x+(buffCount-1)*self.buffSpan+10,y=pos.y-8,width=100,height=17,r=255,g=255,b=255,align="left"})
            table.insert(self.targetBuff[monsterIndex],buff)
        end
    end
    function TargetsUI:RemoveBuffLayer()
        for targetIndex, target in ipairs(self.targets) do
            -- 减少 Buff 层数
            target.buffs = target.buffs or {}
            for buffId, buffLayer in pairs(target.buffs) do
                print("移除Buff" .. targetIndex .. "," .. buffId .. "," .. buffLayer)
                target.buffs[buffId] = buffLayer - 1
                print("移除Buff" .. targetIndex .. "," .. buffId .. "," .. target.buffs[buffId])
            end
            -- 检查并移除无效 Buff
            if self.targetBuff[targetIndex] then
                for i = #self.targetBuff[targetIndex], 1, -1 do
                    local buff = self.targetBuff[targetIndex][i]
                    if buff.buffId and self:GetBuff(targetIndex, buff.buffId) <= 0 then
                        print("移除Buff" .. buff.buffId)
                        table.remove(self.targetBuff[targetIndex], i)
                    end
                end
            end
        end
        for i = 1, #self.targetBuff do
            if self.targetBuff[i] then
                for buffPos,v in ipairs(self.targetBuff[i]) do
                    v.index = buffPos
                    local buffId =  v.buffId
                    local pos =  self.targetsPos[i]
                    v.buffLayer = self.targets[i].buffs[buffId]
                    v.pic:Set({x=pos.x+(buffPos-1)*self.buffSpan,y=pos.y-24,width=17,height=17})
                    v.text:Set({text=tostring( v.buffLayer),x=pos.x+(buffPos-1)*self.buffSpan+10,y=pos.y-8})
                end
            end
        end
        collectgarbage()
    end
    function TargetsUI:Hide(monsterIndex)
        local i = monsterIndex
        self.targetsBox[i].Selected:Hide()
        self.targetsBox[i].BG:Hide()
        self.targetsBox[i].FG:Hide()
        self.targetsText[i].healthMaxHealth:Hide()
        self.targetsText[i].attack:Hide()
        self.targetsPic[i].attackPic:Hide()
        self.targetsPic[i].attackBuffPic:Hide()
        self.targetsPic[i].armorPic:Hide()
        self.targetsPic[i].nextArmorPic:Hide()
        self.targetsText[i].armor:Hide()
        self.targetBuff[i] = nil
        self.selectedSelf:Hide()
        collectgarbage()
    end
    function TargetsUI:Create(screenSize)
        local targetsUI = {}
        setmetatable(targetsUI,self)
        self.__index = self
        targetsUI.screenWidth = screenSize.width
        targetsUI.screenHeight = screenSize.height
        return targetsUI
    end
    function TargetsUI:SetPos(targetNum)
        local length = self.width*targetNum  -  (self.width - self.span)*(targetNum -1)
        local startPos = (self.screenWidth-length)/2
        for i = 1, targetNum  do
            self.targetsPos[i]={x=startPos+(i-1)*self.span ,y=self.startY}
        end
        self.targetNum = targetNum
    end
    --被选中
    function TargetsUI:Selected(index)
        for i = 1, self.targetNum do
            self.targetsBox[i].Selected:Hide()
        end
        self.targetsBox[index].Selected:Show()
    end

    function TargetsUI:SelectedSelf()
        self.selectedSelf:Show()
    end

    function TargetsUI:SelectedAll()
        for i = 1, self.targetNum do
            if self.targets[i].health>0 then
                self.targetsBox[i].Selected:Show()
            end
        end
    end
    function TargetsUI:UnSelected()
        self.selectedSelf:Hide()
        for i = 1, self.targetNum do
            self.targetsBox[i].Selected:Hide()
        end
    end
    function TargetsUI:Init()
        self.selectedSelf = UI.Box.Create()
        self.selectedSelf:Set(self.color[3])
        self.selectedSelf:Set({x=0,y=self.screenHeight-40,width=self.screenWidth,height=40,a=100})
        self.selectedSelf:Hide()
        for i = 1, self.targetNum  do
            local pos =  self.targetsPos[i]
            local HW = {width = self.width,height=self.height}
            self.targetsBox[i]={
                Selected = UI.Box.Create(),
                BG = UI.Box.Create(),
                FG = UI.Box.Create(),
            }
            self.targetsText[i] = {
                healthMaxHealth = UI.Text.Create(),
                attack = UI.Text.Create(),
                armor = UI.Text.Create(),
                --被攻击特效
                attacked = DrawText:Create(),
            }
            local newPos = {x=pos.x-4,y=pos.y-4}
            local newHW = {width=HW.width+8,height=HW.height+8}
            self.targetsBox[i].Selected:Set(newPos)
            self.targetsBox[i].Selected:Set(newHW)
            self.targetsBox[i].Selected:Set(self.color[3])
            self.targetsBox[i].Selected:Hide()
            newPos = {x=pos.x-2,y=pos.y-2}
            newHW = {width=HW.width+4,height=HW.height+4}
            self.targetsBox[i].BG:Set(newPos)
            self.targetsBox[i].BG:Set(newHW)
            self.targetsBox[i].BG:Set(self.color[2])
            self.targetsBox[i].FG:Set(pos)
            self.targetsBox[i].FG:Set(HW)
            self.targetsBox[i].FG:Set(self.color[1])

            newPos = {x=pos.x,y=pos.y+10}
            newHW = {width=HW.width,height=HW.height+3}
            self.targetsText[i].healthMaxHealth:Set(newPos)
            self.targetsText[i].healthMaxHealth:Set(newHW)
            self.targetsText[i].healthMaxHealth:Set({r=255,g=255,b=255,align="center"})
            self.targetsPic[i]={
                attackPic = DrawPicture:Create(),
                attackBuffPic = DrawPicture:Create(),
                armorPic = DrawPicture:Create(),
                nextArmorPic = DrawPicture:Create(),
            }
            self.targetsPic[i].nextArmorPic:Set({picture="armor",x=pos.x,y=pos.y+20,width=18,height=20})
            self.targetsPic[i].nextArmorPic:Hide()

            self.targetsPic[i].attackBuffPic:Set({picture="action",x=pos.x,y=pos.y+20,width=17,height=17})
            self.targetsPic[i].attackBuffPic:Hide()
            self.targetsText[i].attack:Set({r=255,g=255,b=255,width=30,height=30,align="center",x=pos.x+5,y=pos.y+30})

            self.targetsPic[i].attackPic:Set({picture="attack_1",x=pos.x,y=pos.y+20,width=17,height=17})
            self.targetsPic[i].attackPic:Hide()
            self.targetsText[i].attack:Set({r=255,g=255,b=255,width=30,height=30,align="center",x=pos.x+5,y=pos.y+30})

            self.targetsPic[i].armorPic:Set({picture="armor",x=pos.x-30,y=pos.y-5,width=18,height=20})
            self.targetsPic[i].armorPic:Hide()
            self.targetsText[i].armor:Set({r=255,g=255,b=255,width=30,height=30,align="center",x=pos.x-36,y=pos.y})


        end
    end
    function TargetsUI:RoundStart()
        for i = 1, #self.targetsText do
            self.targetsText[i].attack:DoColor({r=255,g=255,b=255,a=0},{r=255,g=255,b=255,a=255},2)
        end
    end
    function TargetsUI:SetDamageText(targetIndex,damage)
        if not damage then return end
        if damage==0 then return end
        local pos =  self.targetsPos[targetIndex]
        self.targetsText[targetIndex].attacked = DrawText:Create()
        self.targetsText[targetIndex].attacked:Set({text=tostring(damage),x=pos.x+30+math.random(-10,10),y=pos.y,size=10})
        local origin=self.targetsText[targetIndex].attacked:Get()
        self.targetsText[targetIndex].attacked:DoScale({width=2,height=2},{width=origin.width*damage*0.2,height=origin.height*damage*0.2},0.5)
        Invoke(function(_self,_targetIndex)
            local _origin =_self.targetsText[_targetIndex].attacked:Get()
            _self.targetsText[_targetIndex].attacked:DoScale({ width= _origin.width, height= _origin.height},{ width=0, height=0},1)
        end,0.2,{self,targetIndex})
        Invoke(function(_self,_targetIndex)
            _self.targetsText[_targetIndex].attacked:Hide()
        end,1,{self,targetIndex})
    end

    function TargetsUI:OnRoundEnd()
        for monsterIndex = 1, #self.targets do
            self:Hide(monsterIndex)
            self.targets[monsterIndex]={}
        end
    end

    PlayerUI={
        buffSpan = 25,
        costUIBGColor ={{r=255,g=184,b=0},{r=166,g=120,b=0}},
        costUIPos = {x=50,y=UI.ScreenSize().height*5/7},
        roundEndBtnPos = {x=UI.ScreenSize().width-200,y=UI.ScreenSize().height*5/7 },
        buffBGInfo = {x=50,y=UI.ScreenSize().height-50-25,r=255,g=255,b=255,a=111,height=19,width=0}
    }

    function PlayerUI:Create(screenSize)
        local playerUI={}
        setmetatable(playerUI,self)
        self.__index = self
        playerUI.width = screenSize.width
        playerUI.height = screenSize.height
        playerUI.buffs = {}
        playerUI.playerBuffs = {}
        playerUI.pos =  {x=50,y=playerUI.height - 50}
        playerUI.buffBG = UI.Box.Create()
        playerUI.costUI = UI.Text.Create()
        playerUI.costUIBG = UI.Box.Create()
        playerUI.buffBG :Set(self.buffBGInfo)
        playerUI.costUIBG :Set(self.costUIPos)
        playerUI.costUIBG :Set(self.costUIBGColor[1])
        playerUI.costUIBG :Set({width=85, height=80})
        playerUI.costUI:Set(self.costUIPos)
        playerUI.costUI:Set({width=85, height=100})
        playerUI.costUIBG:Show()
        playerUI.costUI:Show()

        playerUI.roundEndBtn = UI.Box.Create()
        playerUI.roundEndBtn:Set(self.roundEndBtnPos)
        playerUI.roundEndBtn:Set(self.costUIBGColor[1])
        playerUI.roundEndBtn:Set({width=200, height=80})

        playerUI.roundEndText = DrawText:Create()
        playerUI.roundEndText:Set({text="空格结束回合",size=2})
        local bgSet=playerUI.roundEndBtn:Get()
        local textSet = playerUI.roundEndText:Get()
        playerUI.roundEndText:Set({x=bgSet.x+(bgSet.width-textSet.width)/2,y=bgSet.y+(bgSet.height-textSet.height)/2})

        playerUI.ScreenUIBox = UI.Box.Create()
        playerUI.ScreenUIBox:Set({x=0,y=0,width=UI.ScreenSize().width*2,height=UI.ScreenSize().height*2,r=200,g=0,b=0,a=0})
        return playerUI
    end
    function PlayerUI:SetCostUI(cost,maxCost)
        if cost>0 then
            self.costUIBG:DoColor(self.costUIBGColor[2],self.costUIBGColor[1],0.5)
        else
            self.costUIBG:DoColor(self.costUIBGColor[1],self.costUIBGColor[2],0.5)
        end
        self.costUI:Set({text=tostring(cost.."/"..maxCost) , font="medium", align="center", r=255, g=255, b=255})
    end
    function PlayerUI:GetBuff(buffId)
        if  self.playerBuffs[buffId] then
            return self.playerBuffs[buffId]
        else
            return 0
        end
    end
    function PlayerUI:SetBuff(args)
        local buffId = tonumber(args[1])
        local buffLayer = tonumber(args[2])
        print("SetPlayerBuff"..buffId..","..buffLayer)
        self.playerBuffs[buffId] = buffLayer
        local pos =  self.pos
        local picName = "hurt"
        if buffId==BuffEnum.Hurt then
            picName = "hurt"
        end
        if buffId==BuffEnum.Week then
            picName = "week"
        end
        if buffId==BuffEnum.ToSingle then
            picName = "toSingle"
        end
        if buffId==BuffEnum.ToMultiple then
            picName = "toMultiple"
        end
        if buffId==BuffEnum.AttackPoison then
            picName = "attackPoison"
        end
        if buffId==BuffEnum.AttackPoisonPlus then
            picName = "attackPoisonPlus"
        end
        if buffId==BuffEnum.DefensiveStance then
            picName = "defensiveStance"
        end
        if buffId==BuffEnum.Poison then
            picName = "poison"
        end
        if buffId==BuffEnum.SelfHarm then
            picName = "selfHarm"
        end
        if buffId==BuffEnum.Transfer then
            picName = "transfer"
        end
        if buffId==BuffEnum.Recovery then
            picName = "recovery"
        end
        if buffId==BuffEnum.DamageConservation then
            picName = "damageConservation"
        end
        if buffId==BuffEnum.Reclaim then
            picName = "reclaim"
        end
        if buffId==BuffEnum.Loan then
            picName = "loan"
        end
        if buffId==BuffEnum.ThornMail then
            picName = "thornMail"
        end
        if buffId==BuffEnum.Wounds then
            picName = "wounds"
        end
        if buffId==BuffEnum.SegmentShoot then
            picName = "segmentShoot"
        end
        local hasBuff = false
        for i = 1, #self.buffs do
            if self.buffs[i].buffId==buffId then
                self.buffs[i].text:Set({text=tostring(buffLayer)})
                hasBuff = true
            end
        end
        --已经存在buff图标不再添加图标而是堆叠数字
        if not hasBuff then
            local buffCount = #self.buffs + 1
            local buff={
                picName = picName,
                index = buffCount,
                buffId = buffId,
                pic=DrawPicture:Create(),
                text=UI.Text.Create()
            }
            buff.pic:Set({picture=picName,x=pos.x+(buffCount-1)*self.buffSpan,y=pos.y-24,width=17,height=17})
            buff.text:Set({text=tostring(buffLayer),x=pos.x+(buffCount-1)*self.buffSpan+12,y=pos.y-8,width=17,height=17,r=255,g=255,b=255})
            table.insert(self.buffs,buff)
            self.buffBG :Set({width = #self.buffs*self.buffSpan})
        end
    end
    function PlayerUI:ClearBuff(clearBuffId)
        for buffId, _ in pairs(self.playerBuffs) do
            if buffId == clearBuffId then
                self.playerBuffs[buffId] = 0
            end
        end
        -- 逆序遍历 self.buffs 并直接移除无效的 Buff
        for i = #self.buffs, 1, -1 do
            local buff = self.buffs[i]
            local buffId = buff.buffId
            if buffId and self.playerBuffs[buffId] <= 0 then
                table.remove(self.buffs, i)
            end
        end

        for buffPos,v in ipairs(self.buffs) do
            v.index = buffPos
            local buffId =  v.buffId
            local pos =  self.pos
            v.buffLayer = self.playerBuffs[buffId]
            v.pic:Set({x=pos.x+(buffPos-1)*self.buffSpan,y=pos.y-24,width=17,height=17})
            v.text:Set({text=tostring(v.buffLayer),x=pos.x+(buffPos-1)*self.buffSpan+12,y=pos.y-8})
            collectgarbage()
        end
        self.buffBG :Set({width = #self.buffs*self.buffSpan})
    end
    function PlayerUI:RemoveBuffLayer()
        for buffId,_ in pairs(self.playerBuffs) do
            print("移除玩家Buff"..buffId..",".. self.playerBuffs[buffId])
            self.playerBuffs[buffId] = self.playerBuffs[buffId] - 1
            print("移除玩家Buff"..buffId..",".. self.playerBuffs[buffId])
        end
        -- 逆序遍历 self.buffs 并直接移除无效的 Buff
        for i = #self.buffs, 1, -1 do
            local buff = self.buffs[i]
            local buffId = buff.buffId
            print(buffId)
            print(self:GetBuff(buffId))
            if buffId and self:GetBuff(buffId) <= 0 then
                table.remove(self.buffs, i)
                print("已经移除"..buffId)
            end
        end
        for buffPos,v in ipairs(self.buffs) do
            v.index = buffPos
            local buffId =  v.buffId
            local pos =  self.pos
            v.buffLayer = self.playerBuffs[buffId]
            v.pic:Set({x=pos.x+(buffPos-1)*self.buffSpan,y=pos.y-24,width=17,height=17})
            v.text:Set({text=tostring(v.buffLayer),x=pos.x+(buffPos-1)*self.buffSpan+12,y=pos.y-8})
            collectgarbage()
        end
        self.buffBG :Set({width = #self.buffs*self.buffSpan})
    end
    function PlayerUI:RoundEnd()
        self.roundEndBtn:DoColor(self.costUIBGColor[2],self.costUIBGColor[1],0.5)
    end
    function PlayerUI:Hide()
        self.buffs = nil
        self.costUI = nil
        self.costUIBG= nil
        self.roundEndText= nil
        self.roundEndBtn= nil
        self.ScreenUIBox = nil
        self.buffBG = nil
        collectgarbage()
    end
    function PlayerUI:Attacked()
        if not  self.ScreenUIBox then return end
        self.ScreenUIBox:DoColor({r=200,g=0,b=0,a=100},{r=200,g=0,b=0,a=0},0.5)
    end
    function PlayerUI:Recovery()
        if not self.ScreenUIBox then return end
        self.ScreenUIBox:DoColor({r=110,g=221,b=110,a=100},{r=110,g=221,b=110,a=0},0.5)
    end
    function PlayerUI:AddArmor()
        if not  self.ScreenUIBox then return end
        self.ScreenUIBox:DoColor({r=192,g=192,b=192,a=100},{r=192,g=192,b=192,a=0},2,"OutQuad")
    end
    function PlayerUI:SetRoundEndText(text)
        self.roundEndText:Set({text=text})
    end
    CardsChoice=CardsUI:Create(UI.ScreenSize())
    CardsChoice.startY = CardsChoice.screenHeight -  CardsChoice.cardHeight - 500
    CardsChoice.selectUI = {}
    CardsChoice.emptyState = {}
    CardsChoice.startChoice = false
    function CardsChoice:AddCard(ui,cardIndex)
        local emptyPos = 999
        for i = 1,  self.cardNum  do
            if self.emptyState[i] then
                emptyPos = i
                break
            end
        end
        --选中的已满，将第一个选中的返回手中
        if emptyPos > self.cardNum and ui.choice ~= true  then
            local firstUIData =self.selectUI[1]
            local firstUI=firstUIData[1]
            local firstUIStartPos=firstUIData[2]
            local firstUIEndPos=firstUIData[3]
            firstUI.choice = false
            self:DoMove(firstUI,Vector2:New(firstUIEndPos),Vector2:New(firstUIStartPos))
            self.emptyState[1] = true
            emptyPos = 1
        end
        --已经选中的返回手中
        if ui.choice == true then
            for i = 1,  self.cardNum  do
                local data=self.selectUI[i]
                if data and data[1] == ui then
                    print("返回手中"..tostring(data[1] == ui))
                    local originStartPos=data[2]
                    local originEndPos=data[3]
                    ui.choice = false
                    self:DoMove(ui,Vector2:New(originEndPos),Vector2:New(originStartPos))
                    self.emptyState[i] = true
                end
            end
        else
            --没选中的设为选中，禁止DoMove
            local endPos = self.cardsPos[emptyPos]
            local cardInfo= ui.cardsBox:Get()
            local startPos ={x= cardInfo.x,y=cardInfo.y}
            self:DoMove(ui,Vector2:New(startPos),Vector2:New(endPos))
            self.selectUI[emptyPos] = {ui,startPos ,endPos,cardIndex}
            self.emptyState[emptyPos] = false
            ui.choice = true
        end
    end
    function CardsChoice:EndChoice()
        local cardsIndex={}
        for k,v in pairs(self.selectUI) do
            local ui=v[1]
            local uiStartPos=v[2]
            local uiEndPos=v[3]
            local cardIndex=v[4]
            if ui.choice==true then
                local endPos = {x=0,y=self.screenHeight+10}
                self:DoMove(ui,Vector2:New(uiEndPos),endPos)
                table.insert(cardsIndex,cardIndex)
            end
            ui.choice = false
        end
        self.startChoice = false
        UI.SignalPlus("EndChoice"..table.concat(cardsIndex, ","))
    end
end